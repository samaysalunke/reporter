import pendulum
from airflow import DAG
from airflow.models import Variable
from airflow.operators.email_operator import EmailOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
from operators.mysql_to_csv import MySqlToCsvFile

local_tz = pendulum.timezone("Asia/Kolkata")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 11, 25, tzinfo=local_tz),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

# 0900 IST
dag = DAG('winter_wear', default_args=default_args, schedule_interval='0 9 * * *')

now = datetime.now(local_tz)
current_date_message = now.strftime("%d %B, %Y")
current_date_filename = now.strftime("%Y%m%d")
file_names = ["/tmp/winter_wear_" + current_date_filename + ".csv"]

mysql_to_csv_winter_wear_part_month_task = MySqlToCsvFile(task_id='mysql_to_csv_winter_wear_part_month_task',
                                                          sql="""
                                                        SELECT
                                                        pap.id AS ProductId,
                                                        pap.product,
                                                        c.category,
                                                        IFNULL((SELECT SUM(ps.stock) FROM products ps WHERE ps.parent_prod_id=pap.id AND ps.is_del=0 AND ps.stock>0 GROUP BY pap.id),0) AS 'Current stock',
                                                        (SELECT COUNT(DISTINCT ps.id) FROM products ps WHERE ps.parent_prod_id=pap.id AND ps.is_del=0 AND ps.stock>0 GROUP BY pap.id) AS 'Number of available sizes',
                                                        ROUND(SUM(hp.price-hp.disc),2) AS 'Total month revenue',
                                                        ROUND(SUM(hp.qty),2) AS 'Total month quantity sold',
                                                        ROUND((SUM(hp.price-hp.disc)/(DATEDIFF(DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY), CONCAT(YEAR(DATE_SUB(CONCAT(CURDATE()), INTERVAL 1 DAY)),"-",MONTH(DATE_SUB(CONCAT(CURDATE()), INTERVAL 1 DAY)), "-01 00:00:00"))+1)), 2) AS 'Average revenue',
                                                        ROUND((SUM(hp.qty)/(DATEDIFF(DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY), CONCAT(YEAR(DATE_SUB(CONCAT(CURDATE()), INTERVAL 1 DAY)),"-",MONTH(DATE_SUB(CONCAT(CURDATE()), INTERVAL 1 DAY)), "-01 00:00:00"))+1)), 2) AS 'Average quantity sold',
                                                        ROUND(IFNULL((SELECT SUM(ps.stock) FROM products ps WHERE ps.parent_prod_id=pap.id AND ps.is_del=0 AND ps.stock>0 GROUP BY pap.id),0)*(DATEDIFF(DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY), CONCAT(YEAR(DATE_SUB(CONCAT(CURDATE()), INTERVAL 1 DAY)),"-",MONTH(DATE_SUB(CONCAT(CURDATE()), INTERVAL 1 DAY)), "-01 00:00:00"))+1)/SUM(hp.qty),2) AS DOI
                                                        FROM orders o
                                                        LEFT JOIN order_attributes oa ON oa.order_id = o.id AND oa.priority_reason = "EXCLUSIVE_USER"
                                                        JOIN has_products hp on hp.order_id = o.id
                                                        JOIN products p on p.id = (CASE WHEN hp.sub_prod_id>0 THEN hp.sub_prod_id ELSE hp.prod_id END)
                                                        JOIN products pap on pap.id = (CASE WHEN p.parent_prod_id>0 THEN p.parent_prod_id ELSE p.id END)
                                                        JOIN categories as c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                                        WHERE
                                                        o.order_status NOT IN (0, 17)
                                                        AND c.id IN (512, 682, 663, 664, 637, 637, 635)
                                                        AND o.created_at BETWEEN
                                                        CONCAT(YEAR(DATE_SUB(CONCAT(CURDATE()), INTERVAL 1 DAY)),"-",MONTH(DATE_SUB(CONCAT(CURDATE()), INTERVAL 1 DAY)), "-01 00:00:00")
                                                        AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                        GROUP BY pap.id
                                                        ORDER BY c.category, pap.product, p.id;
                                                      """,
                                                          file_name="/tmp/winter_wear_part_month.csv",
                                                          dag=dag)

mysql_to_csv_winter_wear_part_day_task = MySqlToCsvFile(task_id='mysql_to_csv_winter_wear_part_day_task',
                                                        sql="""
                                                        SELECT
                                                        pap.id AS 'ProductId',
                                                        ROUND(SUM(hp.price-hp.disc),2) AS 'Total day revenue',
                                                        SUM(hp.qty) AS 'Total day quantity sold',
                                                        ROUND(SUM(CASE WHEN oa.id IS NOT NULL THEN (hp.price-hp.disc) ELSE 0 END)*100/SUM((hp.price-hp.disc)),2) AS 'Exclusive revenue percent',
                                                        ROUND(SUM(CASE WHEN oa.id IS NOT NULL THEN hp.qty ELSE 0 END)*100/SUM(hp.qty),2) AS 'Exclusive quantity percent'
                                                        FROM orders o
                                                        LEFT JOIN order_attributes oa ON oa.order_id = o.id AND oa.priority_reason = "EXCLUSIVE_USER"
                                                        JOIN has_products hp on hp.order_id = o.id
                                                        JOIN products p on p.id = (CASE WHEN hp.sub_prod_id>0 THEN hp.sub_prod_id ELSE hp.prod_id END)
                                                        JOIN products pap on pap.id = (CASE WHEN p.parent_prod_id>0 THEN p.parent_prod_id ELSE p.id END)
                                                        JOIN categories as c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                                        WHERE
                                                        o.order_status NOT IN (0, 17)
                                                        AND c.id IN (512, 682, 663, 664, 637, 637, 635)
                                                        AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                        GROUP BY pap.id
                                                        ORDER BY c.category, pap.product, p.id;
                                                      """,
                                                        file_name="/tmp/winter_wear_part_day.csv",
                                                        dag=dag)

date_message = "Date of generation: " + current_date_message + "<br><br>"
body_message = date_message + """
                            Please find attached:<br>
                                1. Winter wear product wise
                            """


def merge_left():
    import pandas as pd
    month_df = pd.read_csv("/tmp/winter_wear_part_month.csv")
    day_df = pd.read_csv("/tmp/winter_wear_part_day.csv")
    merged_left = pd.merge(left=month_df, right=day_df, how='left', left_on='ProductId', right_on='ProductId').fillna(0)
    merged_left.to_csv(file_names[0], index=False)


merge_left = PythonOperator(
    task_id='merge_left',
    python_callable=merge_left,
    dag=dag
)

email_task = EmailOperator(
    to=Variable.get("daily_winter_wear_recipients", deserialize_json=True)["to"],
    cc=Variable.get("daily_winter_wear_recipients", deserialize_json=True)["cc"],
    task_id='send_daily_order_export_email',
    subject='Daily winter wear report',
    html_content=body_message,
    mime_charset='us-ascii',
    files=file_names,
    dag=dag)


def delete_files(file_names):
    import os
    for file in file_names:
        if os.path.exists(file):
            os.remove(file)
            print('File removed: ' + file)
        else:
            print('File doesnt exist: ' + file)


csv_cleanup = PythonOperator(
    task_id='csv_cleanup',
    python_callable=delete_files,
    op_kwargs={'file_names': file_names},
    dag=dag,
)

mysql_to_csv_winter_wear_part_month_task >> merge_left
mysql_to_csv_winter_wear_part_day_task >> merge_left
merge_left >> email_task >> csv_cleanup
