from datetime import datetime, timedelta

import pendulum
from airflow import DAG
from airflow.operators.email_operator import EmailOperator
from airflow.operators.python_operator import PythonOperator
from operators.mysql_to_csv import MySqlToCsvFile
from airflow.models import Variable


local_tz = pendulum.timezone("Asia/Kolkata")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 8, 9, tzinfo=local_tz),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

# 0900 IST
dag = DAG('order_export', default_args=default_args, schedule_interval='0 9 * * *')

now = datetime.now(local_tz)
current_date_message = now.strftime("%d %B, %Y")
current_date_filename = now.strftime("%Y%m%d")
file_names = ["/tmp/order_export_" + current_date_filename + ".csv", "/tmp/multiple_orders_" + current_date_filename + ".csv"]

mysql_to_csv_order_export_task = MySqlToCsvFile(task_id='order_export',
                                                sql="""SELECT o.id AS id, u.email,
                                                            CONCAT(IFNULL(o.address1,""), ", ", IFNULL(o.address2,""), ", ", IFNULL(o.address3,"")) AS recipientAddress,
                                                            o.city as city, o.zone_id as state, o.country_id as country, o.postal_code AS pincode, o.phone_no,
                                                            IFNULL(pm.name,"") AS payment_method, IFNULL(ps.payment_status,"") AS payment_status, os.order_status,
                                                            o.order_amt AS order_amt, o.cod_charges AS cod_charges, o.gifting_charges, o.cashback_used AS cashback_used,
                                                            o.shipping_amt AS shipping_price,
                                                            SUM(hp.disc) AS coupon_discount, IFNULL(c.voucher_code,"") AS coupon_code, o.voucher_amt_used AS voucher_discount, IFNULL(v.voucher_code, "") AS voucher_code,
                                                            ROUND(SUM((hp.price-hp.disc)*hp.gst/100),2) AS gst, o.pay_amt AS pay_amt,
                                                            DATE(o.created_at) AS order_date, cc.name AS courierService, o.shiplabel_tracking_id as trackingId, IFNULL(f.flag_name,"") AS flag_name
                                                        FROM orders o
                                                        JOIN has_products hp ON hp.order_id = o.id
                                                        JOIN users u ON o.user_id = u.id
                                                        JOIN order_status os ON o.order_status = os.id
                                                        LEFT JOIN payment_method pm ON o.payment_method = pm.id
                                                        LEFT JOIN courier_companies cc ON cc.id = o.courier
                                                        LEFT JOIN payment_status ps ON ps.id = o.payment_status
                                                        LEFT JOIN coupons c ON c.id = o.coupon_used
                                                        LEFT JOIN coupons v ON v.id = o.voucher_used
                                                        LEFT JOIN flags f ON f.id = o.flag_id
                                                        WHERE o.order_status IN (1,21,27)
                                                        GROUP BY o.id
                                                        ORDER BY o.id;
                                                          """,
                                                file_name=file_names[0],
                                                dag=dag)

mysql_to_csv_multiple_orders_task = MySqlToCsvFile(task_id='multiple_orders',
                                                   sql="""
                                                  SELECT DISTINCT a.email, a.orders_placed
                                                    FROM
                                                          (
                                                             SELECT DISTINCT u.email, u.id, GROUP_CONCAT(DISTINCT o.id) as orders_placed, GROUP_CONCAT(DISTINCT o1.id) as other_orders
                                                             FROM orders o
                                                             JOIN orders o1 ON o.user_id = o1.user_id AND o.id!=o1.id AND o.postal_code=o1.postal_code AND o.address1=o1.address1 AND o.address2=o1.address2 AND o1.order_status NOT IN (0,4,17) AND o1.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 08:00:00"), INTERVAL 1 DAY) AND CONCAT(CURDATE(), " 07:59:59")
                                                             JOIN users u ON o.user_id = u.id
                                                             WHERE o.order_status NOT IN (0,4,17) AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 08:00:00"), INTERVAL 1 DAY) AND CONCAT(CURDATE(), " 07:59:59")
                                                             AND o.id NOT IN (
                                                               SELECT a.order_id FROM (
                                                                 SELECT o.id as order_id, hp.prod_id AS prod_id
                                                                 FROM has_products hp
                                                                 JOIN orders o on hp.order_id=o.id
                                                                 JOIN order_attributes oa ON oa.order_id = o.id
                                                                 WHERE oa.priority_reason = "EXCLUSIVE_USER" AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 08:00:00"), INTERVAL 1 DAY) AND CONCAT(CURDATE(), " 07:59:59") AND o.order_status NOT IN (0, 17) AND o.payment_method!=1
                                                                 GROUP BY o.id
                                                                 HAVING SUM(hp.qty)=1 AND prod_id IN (147501, 152694)
                                                               ) a
                                                             )
                                                             AND o1.id NOT IN (
                                                               SELECT b.order_id FROM (
                                                                 SELECT o.id as order_id, hp.prod_id AS prod_id
                                                                 FROM has_products hp
                                                                 JOIN orders o on hp.order_id=o.id
                                                                 JOIN order_attributes oa ON oa.order_id = o.id
                                                                 WHERE oa.priority_reason = "EXCLUSIVE_USER" AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 08:00:00"), INTERVAL 1 DAY) AND CONCAT(CURDATE(), " 07:59:59") AND o.order_status NOT IN (0, 17) AND o.payment_method!=1
                                                                 GROUP BY o.id
                                                                 HAVING SUM(hp.qty)=1 AND prod_id IN (147501, 152694)
                                                               ) b
                                                             )
                                                             AND o1.id IS NOT NULL
                                                             GROUP BY u.email
                                                             ORDER BY COUNT(DISTINCT o.id) DESC
                                                         ) a
                                                    ORDER BY a.orders_placed DESC, a.other_orders DESC;

                                                   """,
                                                   file_name=file_names[1],
                                                   dag=dag)

date_message = "Date of generation: " + current_date_message + "<br><br>"
body_message = date_message + """
                            Please find attached:<br>
                                1. Order export (order status - placed, processing, order issue)<br>
                                2. Multiple orders
                            """

email_task = EmailOperator(
    to=Variable.get("daily_order_export_recipients", deserialize_json=True)["to"],
    cc=Variable.get("daily_order_export_recipients", deserialize_json=True)["cc"],
    task_id='send_daily_order_export_email',
    subject='Daily order export',
    html_content=body_message,
    mime_charset='us-ascii',
    files=file_names,
    dag=dag)


def delete_files(file_names):
    import os
    for file in file_names:
        if os.path.exists(file):
            os.remove(file)
            print('File removed: ' + file)
        else:
            print('File doesnt exist: ' + file)


csv_cleanup = PythonOperator(
    task_id='csv_cleanup',
    python_callable=delete_files,
    op_kwargs={'file_names': file_names},
    dag=dag,
)

mysql_to_csv_order_export_task >> email_task
mysql_to_csv_multiple_orders_task >> email_task
email_task >> csv_cleanup
