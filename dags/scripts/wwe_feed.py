import sys

sys.path.append('/usr/local/airflow/dags/')
from operators.get_mysql_cursor import MySqlCursor
from scripts.feed_constants import *
from collections import defaultdict
from xml.dom import minidom

import json
import time
import traceback
from pprint import pprint


class SyncProduct:
    def __init__(self, session):
        self.session = session
        self.time_taken = time.time()

    def print_time_information(self):
        print('Time Elapsed:{} minutes'.format(self.time_taken / 60))

    def get_products(self):
        query = """
                SELECT p.id,
                CASE
                    WHEN c.id = 149 THEN CONCAT(p.product," T-Shirt")
                    WHEN c.id = 531 THEN CONCAT(p.product," T-Shirt Dress")
                    WHEN c.id IN (157, 375) THEN CONCAT(p.product, " Backpack")
                    WHEN c.id = 153 THEN CONCAT(p.product, " ", c1.category, " Mobile Cover")
                ELSE CONCAT(p.product," ", c.category)
                END AS title,
                p.short_desc AS description,
                IFNULL(CASE
                  WHEN p.prod_type = 3 THEN (SELECT SUM(subp.stock) FROM products subp WHERE subp.parent_prod_id = p.id AND subp.stock>0 AND subp.is_del = 0 GROUP BY p.id)
                  WHEN p.prod_type != 3 THEN p.stock
                END,0) AS stock,
                CONCAT("INR ", ROUND(p.price)) AS price,
                CASE WHEN p.spl_price>0 THEN CONCAT("INR ", ROUND(p.spl_price)) ELSE ""
                END AS sale_price,
                p.url_key,
                p.images,
                CASE
                WHEN c.id IN (149,166,531,232) AND p.product LIKE "%Solids%" THEN SUBSTRING(p.product, POSITION("Solids: " IN p.product)+8)
                ELSE "Multi-Color"
                END
                as color,
                CASE
                    WHEN c.id=531 THEN "Female"
                    ELSE "Unisex"
                END  AS gender,
                c.google_category_id,
                IFNULL(add_desc.material, "") as material,
                IFNULL(CASE
                    WHEN c.id IN (149,166,531,232) AND p.product LIKE "%Solids%" THEN "Solid"
                    WHEN c.id IN (149,166,531,232) AND p.product NOT LIKE "%Solids%" THEN "Graphic"
                END, "")
                as pattern,
                c.category as product_type,
                IFNULL((SELECT GROUP_CONCAT(DISTINCT(attr_val)) as size FROM has_options WHERE attr_id = 1 AND prod_id IN (SELECT id FROM products pr WHERE pr.parent_prod_id = p.id)), "One Size") as size,
                IFNULL(GROUP_CONCAT(DISTINCT(IF(tt.tag_name ='', NULL, tt.tag_name))), "") as tags,
                IFNULL(add_desc.desc, "") as add_desc,
                CONCAT(ua.firstname, " ", ua.lastname) AS artist
                FROM products p
                LEFT JOIN users ua ON p.added_by = ua.id
                JOIN categories as c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = p.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                JOIN categories as c1 ON (c1.id = (SELECT cat_id FROM has_categories WHERE prod_id = p.id AND cat_id NOT IN (162,148) ORDER BY id LIMIT 1))
                JOIN tagging_tagged tt ON tt.taggable_id=p.id AND tt.taggable_type = "Product"
                LEFT JOIN add_desc ON add_desc.id = p.add_desc_id
                WHERE p.is_individual = 1 AND p.is_del = 0 AND p.parent_prod_id = 0 AND p.is_avail = 1
                AND p.added_by = 75692
                GROUP BY p.id;
                """

        self.session.execute(query)

        items = self.session.fetchall()

        doc, root = self.initialize_doc()

        for product in items:
            product = dict(product)
            productTitle = sentanceCase(removeDuplicateCategory(pluralToSingular(product['title'])))
            productDescription = sentanceCase(cleanhtml(product['description']))
            if not productDescription:
                productDescription = productTitle
            additionalDescription = striptags(cleanhtml(newline(str(product['add_desc']))))
            if not additionalDescription:
                additionalDescription = productDescription

            stock = int(product['stock'])

            entry = doc.createElement('entry')
            root.appendChild(entry)

            createNode(doc, entry, "id", str(product['id']))
            createNode(doc, entry, "title", productTitle)
            createNode(doc, entry, "description", productDescription.replace('“', '"'))
            createNode(doc, entry, "stock", str(stock) if stock >= 0 else '0')
            createNode(doc, entry, "availability", "in stock" if stock > 0 else "out of stock")
            createNode(doc, entry, "condition", conditionP)
            createNode(doc, entry, "price", product['price'])
            createNode(doc, entry, "link", "https://www.wweshop.in/product/" + product['url_key'])
            createNode(doc, entry, "image_link", str(getImage(product['images'])))
            createNode(doc, entry, "brand", product['artist'].replace("\x99", "").strip() if product['artist'] else "")
            createNode(doc, entry, "additional_image_link", str(getImage1(product['images'])))
            createNode(doc, entry, "age_group", age_group)
            createNode(doc, entry, "color", justColor(product['color']))
            createNode(doc, entry, "gender", product['gender'])
            createNode(doc, entry, "item_group_id", str(product['id']))
            createNode(doc, entry, "google_product_category", str(product['google_category_id']))
            createNode(doc, entry, "material", str(product['material']))
            createNode(doc, entry, "pattern", product['pattern'])
            createNode(doc, entry, "product_type", product['product_type'])
            createNode(doc, entry, "sale_price", product['sale_price'])
            createNode(doc, entry, "sale_price_effective_date", "")
            createNode(doc, entry, "shipping", "")
            createNode(doc, entry, "shipping_weight", "")
            createNode(doc, entry, "size", changeSizeFormat(product['size']))
            createNode(doc, entry, "tags", product['tags'])
            createNode(doc, entry, "add_desc", additionalDescription)
            createNode(doc, entry, "custom_label_2", "")
            createNode(doc, entry, "custom_label_3", "")
            createNode(doc, entry, "custom_label_4", "")
        return doc

    def initialize_doc(self):
        doc = minidom.Document()
        root = doc.createElement('feed')
        root.setAttribute('xmlns', "http://www.w3.org/2005/Atom")
        root.setAttribute('xmlns:g', "http://base.google.com/ns/1.0")
        doc.appendChild(root)

        keyNode = doc.createElement("title")
        valueNode = doc.createTextNode(title)
        keyNode.appendChild(valueNode)
        root.appendChild(keyNode)
        keyNode = doc.createElement("link")
        valueNode = doc.createTextNode("https://www.wweshop.in")
        keyNode.appendChild(valueNode)
        root.appendChild(keyNode)
        return doc, root

    def insert_into_file(self, doc_object):
        filename = '/tmp/wwe_feed.xml'
        xml_str = doc_object.toprettyxml(indent="  ", encoding='utf-8')
        with open(filename, "wb") as f:
            f.write(xml_str)


if __name__ == '__main__':
    print('Building products for wwe')
    mysql_cursor = MySqlCursor(task_id='run_feed')
    cursor = mysql_cursor.get_cursor()
    sync_product = SyncProduct(cursor)
    p = sync_product.get_products()
    sync_product.insert_into_file(p)
    sync_product.time_taken = time.time() - sync_product.time_taken
    sync_product.print_time_information()
