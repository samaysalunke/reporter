import pendulum
from airflow import DAG
from airflow.models import Variable
from airflow.operators.email_operator import EmailOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
from operators.mysql_to_csv import MySqlToCsvFile

local_tz = pendulum.timezone("Asia/Kolkata")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 9, 25, tzinfo=local_tz),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

# 1200 IST
dag = DAG('daily_shop_tss_split', default_args=default_args, schedule_interval='0 9 * * *')

now = datetime.now(local_tz)
current_date_message = now.strftime("%d %B, %Y")
current_date_filename = now.strftime("%Y%m%d")
file_names = ["/tmp/daily_shop_tss_split_" + current_date_filename + ".csv",
              "/tmp/daily_product_wise_" + current_date_filename + ".csv",
              "/tmp/daily_new_vs_repeat_" + current_date_filename + ".csv",
              "/tmp/daily_cod_prepaid_others_" + current_date_filename + ".csv"]

mysql_to_csv_shop_tss_split_task_a = MySqlToCsvFile(task_id='shop_tss_split_task_a',
                                                    sql="""
                                                    SELECT
                                                    'All products' AS '',
                                                    COUNT(CASE WHEN a.is_shop=0 THEN a.order_id END) AS 'TSS order count',
                                                    COUNT(CASE WHEN a.is_shop=0 AND is_exclusive THEN a.order_id END) AS 'TSS exclusive order count',
                                                    COUNT(CASE WHEN a.is_shop=0 AND !is_exclusive THEN a.order_id END) AS 'TSS non exclusive order count',
                                                    ROUND(AVG(CASE WHEN a.is_shop=0 THEN a.price END),2) AS 'TSS AOV',
                                                    COUNT(CASE WHEN a.is_shop=1 THEN a.order_id END) AS 'Shop order count',
                                                    COUNT(CASE WHEN a.is_shop=1 AND is_exclusive THEN a.order_id END) AS 'Shop exclusive order count',
                                                    COUNT(CASE WHEN a.is_shop=1 AND !is_exclusive THEN a.order_id END) AS 'Shop non exclusive order count',
                                                    ROUND(AVG(CASE WHEN a.is_shop=1 THEN a.price END),2) AS 'Shop AOV',
                                                    COUNT(a.order_id) AS 'Overall order count',
                                                    COUNT(CASE WHEN is_exclusive THEN a.order_id END) AS 'Overall exclusive order count',
                                                    COUNT(CASE WHEN !is_exclusive THEN a.order_id END) AS 'Overall non exclusive order count',
                                                    ROUND(AVG(a.price),2) AS 'Overall AOV',
                                                    '' AS 'Shop order count/TSS order count',
                                                    '' AS 'Shop AOV/TSS AOV'
                                                    FROM (
                                                      SELECT CASE WHEN fh.id IS NOT NULL OR o.flag_id=16 THEN 1 ELSE 0 END AS is_shop, o.id as order_id, SUM(hp.price-hp.disc) AS price, o.flag_id, o.order_status, CASE WHEN oa.id IS NOT NULL THEN 1 ELSE 0 END AS is_exclusive, fh.id as fh_id
                                                      FROM orders o
                                                      JOIN has_products hp ON o.id = hp.order_id
                                                      LEFT JOIN order_attributes oa ON o.id = oa.order_id
                                                      LEFT JOIN flag_history fh ON fh.order_id = o.id AND fh.flag_id = 16
                                                    WHERE o.id IN (
                                                        SELECT DISTINCT o.id
                                                        FROM orders o
                                                        JOIN has_products hp ON o.id = hp.order_id
                                                        JOIN users u ON o.user_id = u.id
                                                        JOIN products p ON p.id = (CASE WHEN hp.sub_prod_id>0 THEN hp.sub_prod_id ELSE hp.prod_id END)
                                                        JOIN products pap ON pap.id = (CASE WHEN p.parent_prod_id>0 THEN p.parent_prod_id ELSE p.id END)
                                                        LEFT JOIN order_attributes oa ON o.id = oa.order_id
                                                        WHERE pap.added_by = 75692
                                                        AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                        AND o.order_status NOT IN (0,4,17)
                                                    )
                                                      GROUP BY o.id
                                                    ) a;
                                                   """,
                                                    file_name='/tmp/1a.csv',
                                                    dag=dag)
mysql_to_csv_shop_tss_split_task_b = MySqlToCsvFile(task_id='shop_tss_split_task_b',
                                                    sql="""
                                                    SELECT
                                                    'WWE products',
                                                    COUNT(CASE WHEN a.is_shop=0 THEN a.order_id END) AS tss_order_count,
                                                    COUNT(CASE WHEN a.is_shop=0 AND is_exclusive THEN a.order_id END) AS tss_exclusive_order_count,
                                                    COUNT(CASE WHEN a.is_shop=0 AND !is_exclusive THEN a.order_id END) AS tss_non_exclusive_order_count,
                                                    ROUND(AVG(CASE WHEN a.is_shop=0 THEN a.taxable_price END),2) AS tss_aov,
                                                    COUNT(CASE WHEN a.is_shop=1 THEN a.order_id END) AS shop_order_count,
                                                    COUNT(CASE WHEN a.is_shop=1 AND is_exclusive THEN a.order_id END) AS shop_exclusive_order_count,
                                                    COUNT(CASE WHEN a.is_shop=1 AND !is_exclusive THEN a.order_id END) AS shop_non_exclusive_order_count,
                                                    ROUND(AVG(CASE WHEN a.is_shop=1 THEN a.taxable_price END),2) AS shop_aov,
                                                    COUNT(a.order_id) AS overall_order_count,
                                                    COUNT(CASE WHEN is_exclusive THEN a.order_id END) AS overall_exclusive_order_count,
                                                    COUNT(CASE WHEN !is_exclusive THEN a.order_id END) AS overall_non_exclusive_order_count,
                                                    ROUND(AVG(a.taxable_price),2) AS overall_order_avg,
                                                    COUNT(CASE WHEN a.is_shop = 1 THEN a.order_id END)/COUNT(CASE WHEN a.is_shop=0 THEN a.order_id END) AS 'shop_order_count/tss_order_count',
                                                    ROUND(ROUND(AVG(CASE WHEN a.is_shop=1 THEN a.taxable_price END),2)/ROUND(AVG(CASE WHEN a.is_shop=0 THEN a.taxable_price END),2),2) AS 'shop_aov/tss_aov'
                                                    FROM (
                                                      SELECT CASE WHEN fh.id IS NOT NULL OR o.flag_id=16 THEN 1 ELSE 0 END AS is_shop, o.id as order_id, SUM(hp.price-hp.disc) AS taxable_price, o.flag_id, o.order_status, CASE WHEN oa.id IS NOT NULL THEN 1 ELSE 0 END AS is_exclusive, fh.id as fh_id
                                                      FROM orders o
                                                      JOIN has_products hp ON o.id = hp.order_id
                                                      JOIN users u ON o.user_id = u.id
                                                      JOIN products p ON p.id = (CASE WHEN hp.sub_prod_id>0 THEN hp.sub_prod_id ELSE hp.prod_id END)
                                                      JOIN products pap ON pap.id = (CASE WHEN p.parent_prod_id>0 THEN p.parent_prod_id ELSE p.id END)
                                                      LEFT JOIN order_attributes oa ON o.id = oa.order_id
                                                      LEFT JOIN flag_history fh ON fh.order_id = o.id AND fh.flag_id = 16
                                                      WHERE pap.added_by = 75692
                                                      AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                      AND o.order_status NOT IN (0,4,17)
                                                      GROUP BY o.id
                                                    ) a;
                                                   """,
                                                    file_name='/tmp/1b.csv',
                                                    skip_header=True,
                                                    dag=dag)

product_wise_task_a = MySqlToCsvFile(task_id='product_wise_task_a',
                                     sql="""
                                            SELECT
                                            a.product_id,
                                            a.product,
                                            IFNULL(a.category,"") AS category,
                                            SUM(CASE WHEN a.is_shop = 0 THEN a.qty ELSE 0 END) AS tss_units,
                                            SUM(CASE WHEN a.is_shop = 0 THEN a.sales ELSE 0 END) AS tss_sales,
                                            ROUND(IFNULL(SUM(CASE WHEN a.is_shop = 0 THEN a.sales ELSE 0 END)/SUM(CASE WHEN a.is_shop = 0 THEN a.qty ELSE 0 END),0),2) AS average_tss_selling_price,
                                            SUM(CASE WHEN a.is_shop = 0 THEN a.royalty ELSE 0 END) AS tss_royalty,
                                            SUM(CASE WHEN a.is_shop = 1 THEN a.qty ELSE 0 END) AS shop_units,
                                            SUM(CASE WHEN a.is_shop = 1 THEN a.sales ELSE 0 END) AS shop_sales,
                                            ROUND(IFNULL(SUM(CASE WHEN a.is_shop = 1 THEN a.sales ELSE 0 END)/SUM(CASE WHEN a.is_shop = 1 THEN a.qty ELSE 0 END),0),2) AS average_shop_selling_price,
                                            SUM(CASE WHEN a.is_shop = 1 THEN a.royalty ELSE 0 END) AS shop_royalty,
                                            SUM(a.qty) AS overall_units,
                                            SUM(a.sales) AS overall_sales,
                                            ROUND(IFNULL(SUM(a.sales)/SUM(a.qty),0),2) AS average_overall_selling_price,
                                            SUM(a.royalty) AS overall_royalty
                                            FROM (
                                             SELECT
                                             CASE WHEN fh.id IS NOT NULL OR o.flag_id=16 THEN 1 ELSE 0 END AS is_shop,
                                             pap.id AS product_id,
                                             pap.product, c.category,
                                             SUM(hp.qty) AS qty,
                                             ROUND(SUM(hp.price - hp.disc),2) AS sales,
                                             ROUND(SUM(hp.price - hp.disc)*pap.art_cut/100,2) as royalty,
                                             pap.art_cut AS royalty_percentage,
                                             c.search_sort
                                             FROM orders o
                                             JOIN has_products hp ON o.id = hp.order_id
                                             JOIN users u ON o.user_id = u.id
                                             JOIN products p ON p.id = (CASE WHEN hp.sub_prod_id>0 THEN hp.sub_prod_id ELSE hp.prod_id END)
                                             JOIN products pap ON pap.id = (CASE WHEN p.parent_prod_id>0 THEN p.parent_prod_id ELSE p.id END)
                                             LEFT JOIN categories c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                             LEFT JOIN order_attributes oa ON o.id = oa.order_id
                                             LEFT JOIN flag_history fh ON fh.order_id = o.id AND fh.flag_id = 16
                                             WHERE pap.added_by = 75692
                                             AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                             AND o.order_status NOT IN (0,4,17)
                                             GROUP BY o.id, pap.id
                                            ) a
                                            GROUP BY a.product_id
                                            ORDER BY a.search_sort, a.product;
                                                   """,
                                     file_name="/tmp/2a.csv",
                                     dag=dag)

product_wise_task_b = MySqlToCsvFile(task_id='product_wise_task_b',
                                     sql="""
                                                SELECT
                                                '','','Total',
                                                SUM(b.tss_units) AS total_tss_units,
                                                SUM(b.tss_sales) AS total_tss_sales,
                                                ROUND(IFNULL(SUM(b.tss_sales)/SUM(b.tss_units),0),2) AS total_average_tss_selling_price,
                                                SUM(b.tss_royalty) AS total_tss_royalty,
                                                SUM(b.shop_units) AS total_shop_units,
                                                SUM(b.shop_sales) AS total_shop_sales,
                                                ROUND(IFNULL(SUM(b.shop_sales)/SUM(b.shop_units),0),2) AS total_average_shop_selling_price,
                                                SUM(b.shop_royalty) AS total_shop_royalty,
                                                SUM(b.overall_units) AS total_overall_units,
                                                SUM(b.overall_sales) AS total_overall_sales,
                                                ROUND(IFNULL(SUM(b.overall_sales)/SUM(b.overall_units),0),2) AS total_average_overall_selling_price,
                                                SUM(b.overall_royalty) AS total_overall_royalty
                                                FROM (
                                                  SELECT
                                                  SUM(CASE WHEN a.is_shop = 0 THEN a.qty ELSE 0 END) AS tss_units,
                                                  SUM(CASE WHEN a.is_shop = 0 THEN a.sales ELSE 0 END) AS tss_sales,
                                                  SUM(CASE WHEN a.is_shop = 0 THEN a.royalty ELSE 0 END) AS tss_royalty,
                                                  SUM(CASE WHEN a.is_shop = 1 THEN a.qty ELSE 0 END) AS shop_units,
                                                  SUM(CASE WHEN a.is_shop = 1 THEN a.sales ELSE 0 END) AS shop_sales,
                                                  SUM(CASE WHEN a.is_shop = 1 THEN a.royalty ELSE 0 END) AS shop_royalty,
                                                  SUM(a.qty) AS overall_units,
                                                  SUM(a.sales) AS overall_sales,
                                                  SUM(a.royalty) AS overall_royalty
                                                   FROM (
                                                     SELECT CASE WHEN fh.id IS NOT NULL OR o.flag_id=16 THEN 1 ELSE 0 END AS is_shop,
                                                     pap.id AS product_id, pap.product, c.category,
                                                     SUM(hp.qty) AS qty,
                                                     ROUND(SUM(hp.price-hp.disc),2) AS sales,
                                                     pap.art_cut AS royalty_percentage,
                                                     ROUND(SUM(hp.price - hp.disc)*pap.art_cut/100,2) as royalty,
                                                     c.search_sort
                                                     FROM orders o
                                                     JOIN has_products hp ON o.id = hp.order_id
                                                     JOIN users u ON o.user_id = u.id
                                                     JOIN products p ON p.id = (CASE WHEN hp.sub_prod_id>0 THEN hp.sub_prod_id ELSE hp.prod_id END)
                                                     JOIN products pap ON pap.id = (CASE WHEN p.parent_prod_id>0 THEN p.parent_prod_id ELSE p.id END)
                                                     LEFT JOIN categories c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                                     LEFT JOIN order_attributes oa ON o.id = oa.order_id
                                                     LEFT JOIN flag_history fh ON fh.order_id = o.id AND fh.flag_id = 16
                                                     WHERE pap.added_by = 75692
                                                     AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                     AND o.order_status NOT IN (0,4,17)
                                                     GROUP BY o.id, pap.id
                                                   ) a
                                                   GROUP BY a.product_id
                                                   ORDER BY a.search_sort, a.product
                                                ) b;
                                               """,
                                     file_name="/tmp/2b.csv",
                                     skip_header=True,
                                     dag=dag)

new_vs_repeat_split = MySqlToCsvFile(task_id='new_vs_repeat_split',
                                     sql="""
                                        SELECT
                                        COUNT(CASE WHEN b.old_order_count=0 AND b.is_shop = 0 THEN 1 END) AS tss_new_users,
                                        COUNT(CASE WHEN b.old_order_count>0 AND b.is_shop = 0 THEN 1 END) AS tss_old_users,
                                        ROUND(COUNT(CASE WHEN b.old_order_count=0 AND b.is_shop = 0 THEN 1 END)*100/COUNT(CASE WHEN b.is_shop=0 THEN 1 END),2) AS tss_new_percentage,
                                        COUNT(CASE WHEN b.old_order_count=0 AND b.is_shop = 1 THEN 1 END) AS shop_new_users,
                                        COUNT(CASE WHEN b.old_order_count>0 AND b.is_shop = 1 THEN 1 END) AS shop_old_users,
                                        ROUND(COUNT(CASE WHEN b.old_order_count=0 AND b.is_shop = 1 THEN 1 END)*100/COUNT(CASE WHEN b.is_shop = 1 THEN 1 END),2) AS shop_new_percentage,
                                        COUNT(CASE WHEN b.old_order_count=0 THEN 1 END) AS overall_new_users,
                                        COUNT(CASE WHEN b.old_order_count>0 THEN 1 END) AS overall_old_users,
                                        ROUND(COUNT(CASE WHEN b.old_order_count=0 THEN 1 END)*100/COUNT(1),2) AS overall_new_percentage
                                        FROM (
                                          SELECT COUNT(DISTINCT a.order_id) as old_order_count, o.user_id, CASE WHEN fh.id IS NOT NULL OR o.flag_id=16 THEN 1 ELSE 0 END AS is_shop, o.id as order_id
                                          FROM orders o
                                          JOIN has_products hp ON o.id = hp.order_id
                                          JOIN users u ON o.user_id = u.id
                                          LEFT JOIN (
                                            SELECT uo.id as order_id, uo.created_at, uo.user_id
                                            FROM orders uo
                                            WHERE uo.order_status NOT IN (0,4,17)
                                          ) a ON a.user_id = u.id AND a.order_id!=o.id AND a.created_at<o.created_at
                                          JOIN products p ON p.id = (CASE WHEN hp.sub_prod_id>0 THEN hp.sub_prod_id ELSE hp.prod_id END)
                                          JOIN products pap ON pap.id = (CASE WHEN p.parent_prod_id>0 THEN p.parent_prod_id ELSE p.id END) AND pap.added_by = 75692
                                          LEFT JOIN order_attributes oa ON o.id = oa.order_id
                                          LEFT JOIN flag_history fh ON fh.order_id = o.id AND fh.flag_id = 16
                                          WHERE
                                          o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                          AND o.order_status NOT IN (0,4,17)
                                          GROUP BY o.id
                                        ) b;        
                                        """,
                                     file_name=file_names[2],
                                     dag=dag)

cod_prepaid_others = MySqlToCsvFile(task_id='cod_prepaid_others',
                                    sql="""
                                        SELECT
                                        COUNT(CASE WHEN b.payment_method NOT IN (1,3) AND b.is_shop = 0 THEN b.order_id ELSE NULL END) AS tss_prepaid_count,
                                        COUNT(CASE WHEN b.payment_method = 1 AND b.is_shop = 0 THEN b.order_id ELSE NULL END) AS tss_cod_count,
                                        COUNT(CASE WHEN b.payment_method = 3 AND b.is_shop = 0 THEN b.order_id ELSE NULL END) AS tss_others_count,
                                        ROUND(COUNT(CASE WHEN b.payment_method = 1 AND b.is_shop=0 THEN b.order_id ELSE NULL END)*100/COUNT(CASE WHEN b.is_shop=0 THEN b.order_id ELSE NULL END),2) AS tss_cod_percent,
                                        COUNT(CASE WHEN b.payment_method NOT IN (1,3) AND b.is_shop = 1 THEN b.order_id ELSE NULL END) AS shop_prepaid_count,
                                        COUNT(CASE WHEN b.payment_method = 1 AND b.is_shop = 1 THEN b.order_id ELSE NULL END) AS shop_cod_count,
                                        COUNT(CASE WHEN b.payment_method = 3 AND b.is_shop = 1 THEN b.order_id ELSE NULL END) AS shop_others_count,
                                        ROUND(COUNT(CASE WHEN b.payment_method = 1 AND b.is_shop=1 THEN b.order_id ELSE NULL END)*100/COUNT(CASE WHEN b.is_shop=1 THEN b.order_id ELSE NULL END),2) AS shop_cod_percent,
                                        COUNT(CASE WHEN b.payment_method NOT IN (1,3) THEN b.order_id ELSE NULL END) AS overall_prepaid_count,
                                        COUNT(CASE WHEN b.payment_method = 1 THEN b.order_id ELSE NULL END) AS overall_cod_count,
                                        COUNT(CASE WHEN b.payment_method = 3 THEN b.order_id ELSE NULL END) AS overall_others_count,
                                        ROUND(COUNT(CASE WHEN b.payment_method = 1 THEN b.order_id ELSE NULL END)*100/COUNT(b.order_id),2) AS overall_cod_percent
                                        FROM (
                                          SELECT o.id as order_id, CASE WHEN fh.id IS NOT NULL OR o.flag_id=16 THEN 1 ELSE 0 END AS is_shop, o.payment_method
                                          FROM orders o
                                          JOIN has_products hp ON o.id = hp.order_id
                                          JOIN users u ON o.user_id = u.id
                                          LEFT JOIN (
                                            SELECT uo.id as order_id, uo.created_at, uo.user_id
                                            FROM orders uo
                                            WHERE uo.order_status NOT IN (0,4,17)
                                          ) a ON a.user_id = u.id AND a.order_id!=o.id AND a.created_at<o.created_at
                                          JOIN products p ON p.id = (CASE WHEN hp.sub_prod_id>0 THEN hp.sub_prod_id ELSE hp.prod_id END)
                                          JOIN products pap ON pap.id = (CASE WHEN p.parent_prod_id>0 THEN p.parent_prod_id ELSE p.id END) AND pap.added_by = 75692
                                          LEFT JOIN order_attributes oa ON o.id = oa.order_id
                                          LEFT JOIN flag_history fh ON fh.order_id = o.id AND fh.flag_id = 16
                                          WHERE
                                          o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                          AND o.order_status NOT IN (0,4,17)
                                          GROUP BY o.id
                                        ) b;        
                                        """,
                                    file_name=file_names[3],
                                    dag=dag)


def merge_files(files):
    import os
    import csv

    writer_file = open(files.pop(0), "w")
    writer = csv.writer(writer_file)

    for file in files:
        if os.path.exists(file):
            reader_file = open(file, "r")
            reader = csv.reader(reader_file)

            for row in reader:
                writer.writerow(row)

            reader_file.close()

            print('File merged: ' + file)

        else:
            print('File doesnt exist: ' + file)
    writer_file.close()


merge_files_task_1 = PythonOperator(
    task_id='merge_files_1',
    python_callable=merge_files,
    op_kwargs={'files': [file_names[0], "/tmp/1a.csv", "/tmp/1b.csv"]},
    dag=dag,
)

merge_files_task_2 = PythonOperator(
    task_id='merge_files_2',
    python_callable=merge_files,
    op_kwargs={'files': [file_names[1], "/tmp/2a.csv", "/tmp/2b.csv"]},
    dag=dag,
)

date_message = "Date of generation: " + current_date_message + "<br><br>"
yesterday = (now - timedelta(1)).strftime("%d %B, %Y")
body_message = date_message + """
                            Please find attached:<br>
                                Date range: """ + yesterday + """ 00:00:00 to  """ + yesterday + """ 23:59:59 <br><br>
                                1. Shop-TSS split for WWE orders<br>
                                2. Product wise sales<br>
                                3. New vs repeat users<br>
                                4. COD, prepaid, others<br>
                            """

email_task = EmailOperator(
    to=Variable.get("daily_shop_tss_split_recipients", deserialize_json=True)["to"],
    cc=Variable.get("daily_shop_tss_split_recipients", deserialize_json=True)["cc"],
    task_id='send_email',
    subject='Daily Shop-TSS split',
    html_content=body_message,
    mime_charset='us-ascii',
    files=file_names,
    dag=dag)


def delete_files(file_names):
    import os
    for file in file_names:
        if os.path.exists(file):
            os.remove(file)
            print('File removed: ' + file)
        else:
            print('File doesnt exist: ' + file)


csv_cleanup_task = PythonOperator(
    task_id='csv_cleanup',
    python_callable=delete_files,
    op_kwargs={'file_names': file_names},
    dag=dag,
)

mysql_to_csv_shop_tss_split_task_a >> merge_files_task_1
mysql_to_csv_shop_tss_split_task_b >> merge_files_task_1
product_wise_task_a >> merge_files_task_2
product_wise_task_b >> merge_files_task_2
merge_files_task_1 >> email_task
merge_files_task_2 >> email_task
new_vs_repeat_split >> email_task
cod_prepaid_others >> email_task
email_task >> csv_cleanup_task
