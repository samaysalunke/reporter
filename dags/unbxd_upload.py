from datetime import datetime, timedelta

import pendulum
from airflow import DAG
from airflow.operators.email_operator import EmailOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator
from airflow.hooks.S3_hook import S3Hook
from hooks.s3_extra_args_hook import S3ExtraArgsHook
from airflow.models import Variable

local_tz = pendulum.timezone("Asia/Kolkata")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 9, 23, tzinfo=local_tz),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

# 0000 IST next day
dag = DAG('unbxd_upload', default_args=default_args, schedule_interval='0 0,12 * * *')

file_name = 'wYMqdgFsYETkw7P6'
file_names = ['/tmp/'+file_name+'.json.gz']

run_feed_task = BashOperator(
                        task_id = 'run_feed',
                        bash_command='python3 /usr/local/airflow/dags/scripts/unbxd_feed.py',
                        dag=dag
                    )
compress_file_task = BashOperator(
                        task_id = 'compress_file',
                        bash_command='gzip /tmp/'+file_name+'.json',
                        dag=dag
                    )

body_message = "S3 file uploaded"
email_task = EmailOperator(
    to=Variable.get("unbxd_upload_recipients", deserialize_json=True)["to"],
    cc=Variable.get("unbxd_upload_recipients", deserialize_json=True)["cc"],
    task_id='send_products_file',
    subject='Daily s3 upload file',
    html_content=body_message,
    mime_charset='us-ascii',
    dag=dag)

def upload_to_s3(file_names):
    s3_key = file_name+'.json.gz'
    s3_bucket = Variable.get("s3_bucket_unbxd_feed")
    S3ExtraArgsHook(aws_conn_id='aws_s3', verify=False).load_file_with_extra_args(file_names[0], s3_key, s3_bucket, replace=True, extra_args={'ACL': 'public-read'})

upload_to_s3_task = PythonOperator(
    task_id='upload_to_s3',
    python_callable=upload_to_s3,
    op_kwargs={'file_names': file_names},
    dag=dag,
)


def delete_files(file_names):
    import os
    for file in file_names:
        if os.path.exists(file):
            os.remove(file)
            print('File removed: ' + file)
        else:
            print('File doesnt exist: ' + file)


file_cleanup1_task = PythonOperator(
    task_id='file_cleanup1',
    python_callable=delete_files,
    op_kwargs={'file_names': file_names},
    dag=dag,
)

file_cleanup2_task = PythonOperator(
    task_id='file_cleanup2',
    python_callable=delete_files,
    op_kwargs={'file_names': file_names},
    dag=dag,
)


file_cleanup1_task >> run_feed_task >> compress_file_task >> upload_to_s3_task >> email_task >> file_cleanup2_task
