import json
import pendulum
from airflow import DAG
from airflow.operators.email_operator import EmailOperator
from airflow.operators.mysql_operator import MySqlOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
from operators.custom_http_operator import CustomSimpleHttpOperator
from operators.mysql_to_csv import MySqlToCsvFile
from airflow.models import Variable

local_tz = pendulum.timezone("Asia/Kolkata")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 8, 9, tzinfo=local_tz),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

#1600 IST
dag = DAG('exclusive_expiry', default_args=default_args, schedule_interval='00 16 * * *')

now = datetime.now(local_tz)
current_date_message = now.strftime("%d %B, %Y")
current_date_filename = now.strftime("%Y%m%d")
file_names = ["/tmp/exclusive_expiry_" + current_date_filename + ".csv"]


mysql_to_csv_exclusive_expiry_task = MySqlToCsvFile(task_id='exclusive_expiry',
                                                    sql="""
                                                        SELECT DISTINCT ue.entity ue
                                                        FROM user_eav ue
                                                        INNER JOIN user_eav ue1 ON ue.entity = ue1.entity
                                                        WHERE ue1.attribute = "EXCLUSIVE_MEMBER" AND ue1.is_active = 1 AND ue1.value = 1
                                                        AND ue.attribute="EXCLUSIVE_EXPIRY"
                                                        AND DATE(ue.value)<CURDATE();
                                                    """,
                                                    file_name=file_names[0],
                                                    skip_header=True,
                                                    mysql_conn_id="tss_live",
                                                    dag=dag)

update_user_eav_task = MySqlOperator(task_id='update_user_eav',
                                     sql="""
                                        UPDATE
                                        user_eav ue
                                        INNER JOIN user_eav ue1 ON ue.entity = ue1.entity
                                        SET ue.is_active = 0, ue1.is_active = 0, ue1.value = 2, ue.updated_at = NOW(), ue1.updated_at = NOW()
                                        WHERE ue1.attribute = "EXCLUSIVE_MEMBER" AND ue1.is_active = 1 AND ue1.value = 1
                                        AND ue.attribute="EXCLUSIVE_EXPIRY"
                                        AND DATE(ue.value)<CURDATE();
                                    """,
                                     mysql_conn_id="tss_live",
                                     autocommit=True,
                                     dag=dag)


def push_function():
    import os
    import csv

    expiry_users = []
    file = file_names[0]
    if os.path.exists(file):
        reader_file = open(file, "r")
        reader = csv.reader(reader_file)
        for row in reader:
            print('row', row)
            expiry_users.append(str(row[0]))
        reader_file.close()
        print('Created list from file: ' + file, expiry_users)
    else:
        print('File doesnt exist: ' + file)
    return {"emails": expiry_users}


push_list_of_users_task = PythonOperator(
    task_id='push_list_of_users',
    python_callable=push_function,
    dag=dag
)

trigger_laravel_email_api_task = CustomSimpleHttpOperator(
    task_id='trigger_laravel_email_api',
    http_conn_id='v1_host',
    endpoint='api/v1/email/monthly_expiry',
    method="POST",
    data="{{task_instance.xcom_pull(task_ids=['push_list_of_users'])}}",
    headers={"Content-Type": "application/json"},
    dag=dag
)

date_message = "Date of generation: " + current_date_message + "<br><br>"
body_message = date_message + """
                            Please find attached:<br>
                                1. Membership expired
                            """

email_task = EmailOperator(
    to=Variable.get("exclusive_expiry_recipients", deserialize_json=True)["to"],
    cc=Variable.get("exclusive_expiry_recipients", deserialize_json=True)["cc"],
    task_id='send_daily_exclusive_expiry_email',
    subject='Daily exclusive expiry report',
    html_content=body_message,
    mime_charset='us-ascii',
    files=file_names,
    dag=dag)


def delete_files(file_names):
    import os
    for file in file_names:
        if os.path.exists(file):
            os.remove(file)
            print('File removed: ' + file)
        else:
            print('File doesnt exist: ' + file)


csv_cleanup = PythonOperator(
    task_id='csv_cleanup',
    python_callable=delete_files,
    op_kwargs={'file_names': file_names},
    dag=dag,
)

mysql_to_csv_exclusive_expiry_task >> push_list_of_users_task >> trigger_laravel_email_api_task >> update_user_eav_task >> email_task >> csv_cleanup
