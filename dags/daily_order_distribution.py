import pendulum
from airflow import DAG
from airflow.models import Variable
from airflow.operators.email_operator import EmailOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
from operators.mysql_to_csv import MySqlToCsvFile

local_tz = pendulum.timezone("Asia/Kolkata")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 9, 25, tzinfo=local_tz),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

# 0900 IST
dag = DAG('daily_order_distribution', default_args=default_args, schedule_interval='0 9 * * *')

now = datetime.now(local_tz)
current_date_message = now.strftime("%d %B, %Y")
current_date_filename = now.strftime("%Y%m%d")
file_names = ["/tmp/category_wise_" + current_date_filename + ".csv",
              "/tmp/artist_wise_" + current_date_filename + ".csv",
              "/tmp/category_artist_wise_" + current_date_filename + ".csv",
              "/tmp/artist_category_wise_" + current_date_filename + ".csv",
              "/tmp/summary_" + current_date_filename + ".csv"]

mysql_to_csv_category_wise_part_month_task = MySqlToCsvFile(task_id='mysql_to_csv_category_wise_part_month_task',
                                                            sql="""
                                                                SELECT c.category AS 'Category',
                                                                ROUND(SUM(hp.price-hp.disc),2) AS 'Total month revenue',
                                                                ROUND(SUM(hp.qty),2) AS 'Total month quantity sold',
                                                                ROUND((SUM(hp.price-hp.disc)/(DATEDIFF(DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY), CONCAT(YEAR(DATE_SUB(CONCAT(CURDATE()), INTERVAL 1 DAY)),"-",MONTH(DATE_SUB(CONCAT(CURDATE()), INTERVAL 1 DAY)), "-01 00:00:00"))+1)), 2) AS 'Average revenue',
                                                                ROUND((SUM(hp.qty)/(DATEDIFF(DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY), CONCAT(YEAR(DATE_SUB(CONCAT(CURDATE()), INTERVAL 1 DAY)),"-",MONTH(DATE_SUB(CONCAT(CURDATE()), INTERVAL 1 DAY)), "-01 00:00:00"))+1)), 2) AS 'Average quantity sold'
                                                                FROM orders o
                                                                JOIN has_products hp ON hp.order_id = o.id
                                                                JOIN products p ON p.id = (CASE WHEN hp.sub_prod_id>0 THEN hp.sub_prod_id ELSE hp.prod_id END)
                                                                JOIN products pap ON pap.id = (CASE WHEN p.parent_prod_id>0 THEN p.parent_prod_id ELSE p.id END)
                                                                JOIN categories AS c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                                                JOIN users ua ON ua.id = pap.added_by
                                                                WHERE o.order_status NOT IN (0, 17)
                                                                AND o.created_at BETWEEN
                                                                CONCAT(YEAR(DATE_SUB(CONCAT(CURDATE()), INTERVAL 1 DAY)),"-",MONTH(DATE_SUB(CONCAT(CURDATE()), INTERVAL 1 DAY)), "-01 00:00:00")
                                                                AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                                GROUP BY c.category
                                                                ORDER BY c.search_sort
                                                            """,
                                                            file_name="/tmp/category_wise_part_month.csv",
                                                            dag=dag)

mysql_to_csv_category_wise_part_day_task = MySqlToCsvFile(task_id='mysql_to_csv_category_wise_part_day_task',
                                                          sql="""
                                                    SELECT c.category AS 'Category',
                                                    ROUND(SUM(hp.price-hp.disc),2) AS 'Total day revenue',
                                                    SUM(hp.qty) AS 'Total day quantity sold',
                                                    ROUND(SUM(CASE WHEN oa.id IS NOT NULL THEN (hp.price-hp.disc) ELSE 0 END),2) AS 'Exclusive revenue',
                                                    SUM(CASE WHEN oa.id IS NOT NULL THEN hp.qty ELSE 0 END) AS 'Exclusive quantity sold',
                                                    ROUND(SUM(CASE WHEN oa.id IS NULL THEN (hp.price-hp.disc) ELSE 0 END),2) AS 'Non exclusive revenue',
                                                    SUM(CASE WHEN oa.id IS NULL THEN hp.qty ELSE 0 END) as 'Non exclusive quanity sold',
                                                    ROUND(SUM(CASE WHEN oa.id IS NOT NULL THEN (hp.price-hp.disc) ELSE 0 END)*100/SUM((hp.price-hp.disc)),2) AS 'Exclusive revenue percent',
                                                    ROUND(SUM(CASE WHEN oa.id IS NOT NULL THEN hp.qty ELSE 0 END)*100/SUM(hp.qty),2) AS 'Exclusive quantity percent'
                                                    FROM orders o
                                                    LEFT JOIN order_attributes oa ON oa.order_id = o.id AND oa.priority_reason = "EXCLUSIVE_USER"
                                                    JOIN has_products hp on hp.order_id = o.id
                                                    JOIN products p on p.id = (CASE WHEN hp.sub_prod_id>0 THEN hp.sub_prod_id ELSE hp.prod_id END)
                                                    JOIN products pap on pap.id = (CASE WHEN p.parent_prod_id>0 THEN p.parent_prod_id ELSE p.id END)
                                                    JOIN categories as c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                                    JOIN users ua ON ua.id = pap.added_by
                                                    WHERE
                                                    o.order_status NOT IN (0, 17)
                                                    AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                    GROUP BY c.id
                                                    ORDER BY c.search_sort;
                                                """,
                                                          file_name="/tmp/category_wise_part_day.csv",
                                                          dag=dag)

mysql_to_csv_artist_wise_task = MySqlToCsvFile(task_id='artist_wise',
                                               sql="""
                                                    -- artist_wise_sales
                                                    SELECT ua.firstname AS 'Artist',
                                                    'Total',
                                                    ROUND(SUM(hp.price-hp.disc),2) as total_revenue,
                                                    SUM(hp.qty) as total_qty_sold,
                                                    'Exclusive',
                                                    ROUND(SUM(CASE WHEN oa.id IS NOT NULL THEN (hp.price-hp.disc) ELSE 0 END),2) as exclusive_revenue,
                                                    SUM(CASE WHEN oa.id IS NOT NULL THEN hp.qty ELSE 0 END) as exclusive_qty_sold,
                                                    'Non Exclusive',
                                                    ROUND(SUM(CASE WHEN oa.id IS NULL THEN (hp.price-hp.disc) ELSE 0 END),2) as non_exclusive_revenue,
                                                    SUM(CASE WHEN oa.id IS NULL THEN hp.qty ELSE 0 END) as non_exclusive_qty_sold,
                                                    'Exclusive Revenue %',
                                                    ROUND(SUM(CASE WHEN oa.id IS NOT NULL THEN (hp.price-hp.disc) ELSE 0 END)*100/SUM((hp.price-hp.disc)),2) as exclusive_revenue_percent,
                                                    'Exclusive Quantity %',
                                                    ROUND(SUM(CASE WHEN oa.id IS NOT NULL THEN hp.qty ELSE 0 END)*100/SUM(hp.qty),2) as exclusive_qty_percent
                                                    FROM orders o
                                                    LEFT JOIN order_attributes oa ON oa.order_id = o.id AND oa.priority_reason = "EXCLUSIVE_USER"
                                                    JOIN has_products hp on hp.order_id = o.id
                                                    JOIN products p on p.id = (CASE WHEN hp.sub_prod_id>0 THEN hp.sub_prod_id ELSE hp.prod_id END)
                                                    JOIN products pap on pap.id = (CASE WHEN p.parent_prod_id>0 THEN p.parent_prod_id ELSE p.id END)
                                                    JOIN categories as c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                                    JOIN users ua ON ua.id = pap.added_by
                                                    WHERE
                                                    o.order_status NOT IN (0, 17)
                                                    AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                    GROUP BY ua.id
                                                    ORDER BY ua.firstname;

                                                """,
                                               file_name=file_names[1],
                                               skip_header=True,
                                               dag=dag)

mysql_to_csv_category_artist_wise_task = MySqlToCsvFile(task_id='category_artist_wise',
                                                        sql="""
                                                    -- category_artist_wise_sales
                                                    SELECT c.category, ua.firstname AS 'Artist',
                                                    'Total',
                                                    ROUND(SUM(hp.price-hp.disc),2) as total_revenue,
                                                    SUM(hp.qty) as total_qty_sold,
                                                    'Exclusive',
                                                    ROUND(SUM(CASE WHEN oa.id IS NOT NULL THEN (hp.price-hp.disc) ELSE 0 END),2) as exclusive_revenue,
                                                    SUM(CASE WHEN oa.id IS NOT NULL THEN hp.qty ELSE 0 END) as exclusive_qty_sold,
                                                    'Non Exclusive',
                                                    ROUND(SUM(CASE WHEN oa.id IS NULL THEN (hp.price-hp.disc) ELSE 0 END),2) as non_exclusive_revenue,
                                                    SUM(CASE WHEN oa.id IS NULL THEN hp.qty ELSE 0 END) as non_exclusive_qty_sold,
                                                    'Exclusive Revenue %',
                                                    ROUND(SUM(CASE WHEN oa.id IS NOT NULL THEN (hp.price-hp.disc) ELSE 0 END)*100/SUM((hp.price-hp.disc)),2) as exclusive_revenue_percent,
                                                    'Exclusive Quantity %',
                                                    ROUND(SUM(CASE WHEN oa.id IS NOT NULL THEN hp.qty ELSE 0 END)*100/SUM(hp.qty),2) as exclusive_qty_percent
                                                    FROM orders o
                                                    LEFT JOIN order_attributes oa ON oa.order_id = o.id AND oa.priority_reason = "EXCLUSIVE_USER"
                                                    JOIN has_products hp on hp.order_id = o.id
                                                    JOIN products p on p.id = (CASE WHEN hp.sub_prod_id>0 THEN hp.sub_prod_id ELSE hp.prod_id END)
                                                    JOIN products pap on pap.id = (CASE WHEN p.parent_prod_id>0 THEN p.parent_prod_id ELSE p.id END)
                                                    JOIN categories as c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                                    JOIN users ua ON ua.id = pap.added_by
                                                    WHERE
                                                    o.order_status NOT IN (0, 17)
                                                    AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                    GROUP BY c.id, ua.id
                                                    ORDER BY c.search_sort, c.category, ua.firstname;
                                                """,
                                                        file_name=file_names[2],
                                                        skip_header=True,
                                                        dag=dag)

mysql_to_csv_artist_category_wise_task = MySqlToCsvFile(task_id='artist_category_wise',
                                                        sql="""
                                                    -- artist_category_wise_sales
                                                    SELECT ua.firstname AS 'Artist', c.category,
                                                    'Total',
                                                    ROUND(SUM(hp.price-hp.disc),2) as total_revenue,
                                                    SUM(hp.qty) as total_qty_sold,
                                                    'Exclusive',
                                                    ROUND(SUM(CASE WHEN oa.id IS NOT NULL THEN (hp.price-hp.disc) ELSE 0 END),2) as exclusive_revenue,
                                                    SUM(CASE WHEN oa.id IS NOT NULL THEN hp.qty ELSE 0 END) as exclusive_qty_sold,
                                                    'Non Exclusive',
                                                    ROUND(SUM(CASE WHEN oa.id IS NULL THEN (hp.price-hp.disc) ELSE 0 END),2) as non_exclusive_revenue,
                                                    SUM(CASE WHEN oa.id IS NULL THEN hp.qty ELSE 0 END) as non_exclusive_qty_sold,
                                                    'Exclusive Revenue %',
                                                    ROUND(SUM(CASE WHEN oa.id IS NOT NULL THEN (hp.price-hp.disc) ELSE 0 END)*100/SUM((hp.price-hp.disc)),2) as exclusive_revenue_percent,
                                                    'Exclusive Quantity %',
                                                    ROUND(SUM(CASE WHEN oa.id IS NOT NULL THEN hp.qty ELSE 0 END)*100/SUM(hp.qty),2) as exclusive_qty_percent
                                                    FROM orders o
                                                    LEFT JOIN order_attributes oa ON oa.order_id = o.id AND oa.priority_reason = "EXCLUSIVE_USER"
                                                    JOIN has_products hp on hp.order_id = o.id
                                                    JOIN products p on p.id = (CASE WHEN hp.sub_prod_id>0 THEN hp.sub_prod_id ELSE hp.prod_id END)
                                                    JOIN products pap on pap.id = (CASE WHEN p.parent_prod_id>0 THEN p.parent_prod_id ELSE p.id END)
                                                    JOIN categories as c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                                    JOIN users ua ON ua.id = pap.added_by
                                                    WHERE
                                                    o.order_status NOT IN (0, 17)
                                                    AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                    GROUP BY ua.id, c.id
                                                    ORDER BY ua.firstname, c.search_sort, c.category;
                                                """,
                                                        file_name=file_names[3],
                                                        skip_header=True,
                                                        dag=dag)

mysql_to_csv_total_task = MySqlToCsvFile(task_id='total',
                                         sql="""
                                                        SELECT 'Total orders', COUNT(o.id) AS total_orders, 'Total amount', ROUND(SUM(o.pay_amt),2) AS total_amount
                                                        FROM orders o
                                                        WHERE o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                        AND o.order_status NOT IN (0, 17);
                                                        """,
                                         file_name="/tmp/total.csv",
                                         skip_header=True,
                                         dag=dag)

mysql_to_csv_payment_status_task = MySqlToCsvFile(task_id='payment_status',
                                                  sql="""
                                                SELECT 'Payment Method',
                                                    'Prepaid', COUNT(CASE WHEN o.payment_method NOT IN (1,3) THEN o.id ELSE NULL END) AS 'prepaid_count',
                                                    'COD', COUNT(CASE WHEN o.payment_method = 1 THEN o.id ELSE NULL END) AS 'cod_count',
                                                    'Others', COUNT(CASE WHEN o.payment_method = 3 THEN o.id ELSE NULL END) AS 'others_count',
                                                    'COD %', ROUND(COUNT(CASE WHEN o.payment_method = 1 THEN o.id ELSE NULL END)*100/COUNT(o.id),2) AS 'cod_percent'
                                                    FROM orders o
                                                    WHERE o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                    AND o.order_status NOT IN (0, 17)
                                                    UNION ALL
                                                    SELECT 'Status',
                                                    'Success', COUNT(CASE WHEN o.order_status NOT IN (0) THEN o.id ELSE NULL END) AS 'success_count',
                                                    'Failed', COUNT(CASE WHEN o.order_status = 17  OR (o.order_status = 0 AND o.payment_status=7) THEN o.id ELSE NULL END) AS 'failure_count',
                                                    'Cancelled', COUNT(CASE WHEN o.order_status = 4 THEN o.id ELSE NULL END) AS 'cancelled_count',
                                                    'Failed %', ROUND(COUNT(CASE WHEN o.order_status = 17 OR (o.order_status = 0 AND o.payment_status=7) THEN o.id ELSE NULL END)*100/COUNT(o.id),2) AS 'failed_percent'
                                                    FROM orders o
                                                    WHERE o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY);
                                                """,
                                                  file_name="/tmp/payment_status.csv",
                                                  skip_header=True,
                                                  dag=dag)

mysql_to_csv_memberships_purchased_task = MySqlToCsvFile(task_id='memberships_purchased',
                                                         sql="""
                                                            SELECT 'Exclusive', 'Memberships Purchased', COUNT(DISTINCT o.id) AS memberships_purchased
                                                            FROM has_products hp
                                                            JOIN orders o on hp.order_id=o.id
                                                            WHERE o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                            AND o.order_status NOT IN (0, 4, 17)
                                                            AND hp.prod_id IN (SELECT id FROM products WHERE prod_type = 5 AND is_avail = 1 AND is_del = 0);
                                                        """,
                                                         file_name="/tmp/memberships_purchased.csv",
                                                         skip_header=True,
                                                         dag=dag)

mysql_to_csv_total_exclusive_orders_task = MySqlToCsvFile(task_id='total_exclusive_orders',
                                                          sql="""
                                                            SELECT 'Exclusive Orders', COUNT(DISTINCT o.id) AS exclusive_orders
                                                            FROM has_products hp
                                                            JOIN orders o on hp.order_id=o.id
                                                            JOIN order_attributes oa ON oa.order_id = o.id AND oa.priority_reason = "EXCLUSIVE_USER"
                                                            WHERE o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                            AND o.order_status NOT IN (0, 17);
                                                            """,
                                                          file_name="/tmp/total_exclusive_orders.csv",
                                                          skip_header=True,
                                                          dag=dag)

mysql_to_csv_only_memberships_purchased_task = MySqlToCsvFile(task_id='only_memberships_purchased',
                                                              sql="""
                                                            SELECT 'Only Exclusive Purchased',
                                                            COUNT(a.order_id) AS only_membership_purchased, 'Only E%'
                                                            FROM
                                                            (
                                                              SELECT o.id as order_id, hp.prod_id AS prod_id
                                                              FROM has_products hp
                                                              JOIN orders o on hp.order_id=o.id
                                                              JOIN order_attributes oa ON oa.order_id = o.id
                                                              WHERE oa.priority_reason = "EXCLUSIVE_USER" AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY) AND o.order_status NOT IN (0, 4, 17)
                                                              GROUP BY o.id
                                                              HAVING SUM(hp.qty)=1 AND prod_id IN (SELECT id FROM products WHERE prod_type = 5 AND is_avail = 1 AND is_del = 0)
                                                            ) a
                                                            """,
                                                              file_name="/tmp/only_memberships_purchased.csv",
                                                              skip_header=True,
                                                              dag=dag)

mysql_to_csv_customers_new_task = MySqlToCsvFile(task_id='customers_new',
                                                 sql="""
                                                            SELECT 'Customers', 'New customers', COUNT(DISTINCT b.email) as new_customers
                                                            FROM (
                                                              SELECT DISTINCT u.email, COUNT(DISTINCT a.order_id) as old_order_count
                                                              FROM orders o
                                                              JOIN users u ON o.user_id = u.id
                                                              LEFT JOIN (
                                                                SELECT uo.id as order_id, uo.created_at, uo.user_id
                                                                FROM orders uo
                                                                WHERE uo.order_status NOT IN (0, 17)
                                                              ) a ON a.user_id = u.id AND a.order_id!=o.id AND a.created_at<o.created_at
                                                              WHERE o.order_status NOT IN (0, 17)
                                                              AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                              GROUP BY u.email
                                                              HAVING COUNT(DISTINCT a.order_id) = 0) b;
                                                        """,
                                                 file_name="/tmp/customers_new.csv",
                                                 skip_header=True,
                                                 dag=dag)

mysql_to_csv_customers_new_exclusive_task = MySqlToCsvFile(task_id='customers_new_exclusive',
                                                           sql="""
                                                            SELECT 'New exclusive members', COUNT(DISTINCT b.email) as new_exclusive_members
                                                              FROM (
                                                                SELECT DISTINCT u.email
                                                                FROM orders o
                                                                JOIN has_products hp ON hp.order_id = o.id
                                                                JOIN users u ON o.user_id = u.id
                                                                WHERE o.order_status NOT IN (0, 4, 17)
                                                                AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                                AND hp.prod_id IN (SELECT id FROM products WHERE prod_type=5 AND is_avail=1 AND is_del=0)
                                                                GROUP BY u.email
                                                              ) b;
                                                            """,
                                                           file_name="/tmp/customers_new_exclusive.csv",
                                                           skip_header=True,
                                                           dag=dag)

mysql_to_csv_customers_new_and_exclusive_order_task = MySqlToCsvFile(task_id='customers_new_and_exclusive_order',
                                                                     sql="""
                                                            SELECT 'New customer and exclusive order', COUNT(DISTINCT b.email) as new_customers, '% of 1st time excl users'
                                                            FROM (
                                                            SELECT DISTINCT u.email, COUNT(DISTINCT a.order_id) as old_order_count
                                                            FROM orders o
                                                            JOIN order_attributes oa ON o.id = oa.order_id AND oa.priority_reason="EXCLUSIVE_USER"
                                                            JOIN users u ON o.user_id = u.id
                                                            LEFT JOIN (
                                                              SELECT uo.id as order_id, uo.created_at, uo.user_id
                                                              FROM orders uo
                                                              WHERE uo.order_status NOT IN (0, 17)
                                                            ) a ON a.user_id = u.id AND a.order_id!=o.id AND a.created_at<o.created_at
                                                            WHERE o.order_status NOT IN (0, 4, 17)
                                                            AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                            GROUP BY u.email
                                                            HAVING COUNT(DISTINCT a.order_id) = 0) b;
                                                            """,
                                                                     file_name="/tmp/customers_new_and_exclusive_order.csv",
                                                                     skip_header=True,
                                                                     dag=dag)

mysql_to_csv_memberships_purchased_row_task = MySqlToCsvFile(task_id='memberships_purchased_row',
                                                             sql="""
                                                                SELECT 'Memberships Purchased', 'TSS', COUNT(DISTINCT CASE WHEN fh.id IS NULL AND o.flag_id!=16 THEN o.id END) AS memberships_purchased_tss, 'Shop', COUNT(DISTINCT CASE WHEN fh.id IS NOT NULL OR o.flag_id = 16 THEN o.id END) AS memberships_purchased_shop, 'Total', COUNT(DISTINCT o.id) AS memberships_purchased_total, 'Shop %', ROUND(COUNT(DISTINCT CASE WHEN fh.id IS NOT NULL OR o.flag_id = 16 THEN o.id END)*100/COUNT(DISTINCT o.id),2) AS shop_percentage  
                                                                FROM orders o
                                                                JOIN has_products hp on hp.order_id=o.id
                                                                LEFT JOIN flag_history fh ON fh.order_id = o.id AND fh.flag_id = 16
                                                                WHERE o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                                AND o.order_status NOT IN (0, 4, 17)
                                                                AND hp.prod_id IN (SELECT id FROM products WHERE prod_type = 5 AND is_avail = 1 AND is_del = 0);
                                                            """,
                                                             file_name="/tmp/memberships_purchased_row.csv",
                                                             skip_header=True,
                                                             dag=dag)


def merge_left():
    import pandas as pd
    month_df = pd.read_csv("/tmp/category_wise_part_month.csv")
    day_df = pd.read_csv("/tmp/category_wise_part_day.csv")
    merged_left = pd.merge(left=month_df, right=day_df, how='left', left_on='Category', right_on='Category').fillna(0)
    merged_left.to_csv(file_names[0], index=False)


merge_left = PythonOperator(
    task_id='merge_left',
    python_callable=merge_left,
    dag=dag
)


def append_to_file(files, summary_cells):
    import os
    import csv

    open(files[0], 'w').close()
    writer_file = open(files.pop(0), "a")

    summary_list = []
    for file in files:
        if os.path.exists(file):
            reader_file = open(file, "r")
            reader = csv.reader(reader_file)
            for row in reader:
                summary_list = summary_list + row
                row_string = ','.join(map(str, row))
                writer_file.write(row_string + ',')
            reader_file.close()
            print('File appended: ' + file)
        else:
            print('File doesnt exist: ' + file)
    print(summary_list)
    print(summary_cells['numerator'])
    writer_file.write(
        str(round(int(summary_list[summary_cells['numerator']]) * 100 / int(summary_list[summary_cells['denominator']]),
                  2))
    )
    writer_file.close()


form_customers_row = PythonOperator(
    task_id='form_customers_row',
    python_callable=append_to_file,
    op_kwargs={'files': ["/tmp/2.csv", "/tmp/customers_new.csv", "/tmp/customers_new_exclusive.csv",
                         "/tmp/customers_new_and_exclusive_order.csv"],
               'summary_cells': {'numerator': 6, 'denominator': 4}},
    dag=dag,
)

form_exclusive_row = PythonOperator(
    task_id='form_exclusive_row',
    python_callable=append_to_file,
    op_kwargs={'files': ["/tmp/1.csv", "/tmp/memberships_purchased.csv", "/tmp/total_exclusive_orders.csv",
                         "/tmp/only_memberships_purchased.csv"], 'summary_cells': {'numerator': 6, 'denominator': 2}},
    dag=dag,
)


def merge_files(files):
    import os
    import csv

    writer_file = open(files.pop(0), "w")
    writer = csv.writer(writer_file)

    for file in files:
        if os.path.exists(file):
            reader_file = open(file, "r")
            reader = csv.reader(reader_file)

            for row in reader:
                writer.writerow(row)

            reader_file.close()

            print('File merged: ' + file)

        else:
            print('File doesnt exist: ' + file)
    writer_file.close()


merge_files_task = PythonOperator(
    task_id='merge_files',
    python_callable=merge_files,
    op_kwargs={
        'files': [file_names[4], "/tmp/total.csv", "/tmp/payment_status.csv", "/tmp/2.csv", "/tmp/1.csv",
                  "/tmp/memberships_purchased_row.csv"]},
    dag=dag,
)

date_message = "Date of generation: " + current_date_message + "<br><br>"
yesterday = (now - timedelta(1)).strftime("%d %B, %Y")
body_message = date_message + """
                            Please find attached:<br>
                                Date range: """ + yesterday + """ 00:00:00 to  """ + yesterday + """ 23:59:59 <br><br>
                                Order distribution based on:<br>
                                1. Category <br>
                                2. Artist <br>
                                3. Category - artist<br>
                                4. Artist - category<br>
                                5. Summary
                            """

email_task = EmailOperator(
    to=Variable.get("daily_order_distribution_recipients", deserialize_json=True)["to"],
    cc=Variable.get("daily_order_distribution_recipients", deserialize_json=True)["cc"],
    task_id='send_daily_order_distribution_email',
    subject='Daily order distribution',
    html_content=body_message,
    mime_charset='us-ascii',
    files=file_names,
    dag=dag)


def delete_files(file_names):
    import os
    for file in file_names:
        if os.path.exists(file):
            os.remove(file)
            print('File removed: ' + file)
        else:
            print('File doesnt exist: ' + file)


csv_cleanup_task = PythonOperator(
    task_id='csv_cleanup',
    python_callable=delete_files,
    op_kwargs={'file_names': file_names},
    dag=dag,
)

mysql_to_csv_memberships_purchased_task >> form_exclusive_row
mysql_to_csv_total_exclusive_orders_task >> form_exclusive_row
mysql_to_csv_only_memberships_purchased_task >> form_exclusive_row

mysql_to_csv_customers_new_task >> form_customers_row
mysql_to_csv_customers_new_exclusive_task >> form_customers_row
mysql_to_csv_customers_new_and_exclusive_order_task >> form_customers_row

form_exclusive_row >> merge_files_task
form_customers_row >> merge_files_task
mysql_to_csv_total_task >> merge_files_task
mysql_to_csv_payment_status_task >> merge_files_task
mysql_to_csv_memberships_purchased_row_task >> merge_files_task

mysql_to_csv_category_wise_part_month_task >> merge_left
mysql_to_csv_category_wise_part_day_task >> merge_left

merge_left >> email_task
mysql_to_csv_artist_wise_task >> email_task
mysql_to_csv_category_artist_wise_task >> email_task
mysql_to_csv_artist_category_wise_task >> email_task

merge_files_task >> email_task
email_task >> csv_cleanup_task