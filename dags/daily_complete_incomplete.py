from datetime import datetime, timedelta

import pendulum
from airflow import DAG
from airflow.operators.email_operator import EmailOperator
from airflow.operators.python_operator import PythonOperator
from operators.mysql_to_csv import MySqlToCsvFile
from airflow.models import Variable

local_tz = pendulum.timezone("Asia/Kolkata")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 8, 9, tzinfo=local_tz),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

# 1201 IST
dag = DAG('complete_incomplete', default_args=default_args, schedule_interval='1 12 * * *')

now = datetime.now(local_tz)
current_date_message = now.strftime("%d %B, %Y")
current_date_filename = now.strftime("%Y%m%d")
file_names = ["/tmp/complete_" + current_date_filename + ".csv", "/tmp/incomplete_" + current_date_filename + ".csv"]

mysql_to_csv_complete_task = MySqlToCsvFile(task_id='complete',
                                            sql="""
                                            SELECT
                                            o.id AS OrderId,
                                            u.id AS UserId,
                                            o.country_id AS Country,
                                            o.zone_id AS State, o.city AS City, o.postal_code AS Pincode,
                                            pap.id AS ParentProductId,
                                            pap.product AS ParentProduct,
                                            p.id AS VariantProductId,
                                            p.product AS VariantProduct,
                                            hp.qty AS Qty,
                                            c.category AS ProductCategory,
                                            hp.price AS ProductPrice,
                                            (hp.price-hp.disc) AS TaxablePrice,
                                            pap.cog*hp.qty AS CostOfGood,
                                            hp.gst AS GST,
                                            CASE WHEN oa.id IS NULL THEN 'Not exclusive' ELSE 'Exclusive' END AS IsExclusive,
                                            ps.payment_status AS PaymentStatus,
                                            os.order_status AS OrderStatus,
                                            pm.name AS PaymentMethod,
                                            o.order_amt AS OrderAmount,
                                            o.cod_charges AS CodCharges,
                                            o.gifting_charges AS GiftingCharges,
                                            o.cashback_used AS RewarPointsUsed,
                                            o.shipping_amt AS ShippingAmt,
                                            o.coupon_amt_used AS DiscountCoupon,
                                            o.voucher_amt_used AS VoucherDiscount,
                                            o.pay_amt AS FinalAmt,
                                            o.order_comment AS OrderComment,
                                            DATE_FORMAT(o.created_at,'%d %b %y') AS OrderDate,
                                            o.created_at AS OrderTimestamp,
                                            o.shiplabel_tracking_id AS ShiplabelTrackingId,
                                            cc.name AS Courier,
                                            pap.added_by AS ArtistId,
                                            ua.firstname AS ArtistName
                                            FROM has_products hp
                                            JOIN orders o ON o.id = hp.order_id
                                            LEFT JOIN order_attributes oa ON o.id = oa.order_id
                                            LEFT JOIN users u ON o.user_id = u.id
                                            JOIN products p ON p.id = (CASE WHEN hp.sub_prod_id>0 THEN hp.sub_prod_id ELSE hp.prod_id END)
                                            JOIN products pap ON pap.id = (CASE WHEN p.parent_prod_id>0 THEN p.parent_prod_id ELSE p.id END)
                                            LEFT JOIN categories AS c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                            LEFT JOIN users ua ON ua.id = pap.added_by
                                            LEFT JOIN order_status os ON o.order_status = os.id
                                            LEFT JOIN payment_method pm ON o.payment_method = pm.id
                                            LEFT JOIN payment_status ps ON o.payment_status = ps.id
                                            LEFT JOIN courier_companies cc ON o.courier = cc.id
                                            WHERE o.order_status NOT IN (0,17)
                                            AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 10:00:00"), INTERVAL 1 DAY) AND CONCAT(CURDATE(), " 09:59:59")
                                            ORDER BY o.created_at ASC;
                                                          """,
                                            file_name=file_names[0],
                                            dag=dag)

mysql_to_csv_incomplete_task = MySqlToCsvFile(task_id='incomplete',
                                              sql="""
                                                    SELECT
                                                      DATE(o.created_at) AS OrderDate,
                                                      o.id AS OrderId,
                                                      u.id AS UserId,
                                                      pm.name AS PaymentMethod,
                                                      o.pay_amt AS FinalAmount
                                                    FROM orders o
                                                    LEFT JOIN users u on o.user_id = u.id
                                                    LEFT JOIN payment_method pm on o.payment_method = pm.id
                                                    WHERE o.order_status IN (0,17)
                                                    AND o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 10:00:00"), INTERVAL 1 DAY) AND CONCAT(CURDATE(), " 09:59:59")
                                                    GROUP BY o.id
                                                    ORDER BY o.created_at ASC;
                                                   """,
                                              file_name=file_names[1],
                                              dag=dag)

date_message = "Date of generation: " + current_date_message + "<br><br>"
yesterday = (now - timedelta(1)).strftime("%d %B, %Y")

body_message = date_message + """
                            Please find attached:<br>
                                Date range: """ +  yesterday + """ 10:00:00 to  """ + current_date_message + """ 09:59:59 <br><br>
                                1. Complete orders <br>
                                2. Incomplete orders
                            """

email_task = EmailOperator(
    to=Variable.get("daily_complete_incomplete_recipients", deserialize_json=True)["to"],
    cc=Variable.get("daily_complete_incomplete_recipients", deserialize_json=True)["cc"],
    task_id='send_daily_complete_incomplete_email',
    subject='Daily complete incomplete order export',
    html_content=body_message,
    mime_charset='us-ascii',
    files=file_names,
    dag=dag)


def delete_files(file_names):
    import os
    for file in file_names:
        if os.path.exists(file):
            os.remove(file)
            print('File removed: ' + file)
        else:
            print('File doesnt exist: ' + file)


csv_cleanup = PythonOperator(
    task_id='csv_cleanup',
    python_callable=delete_files,
    op_kwargs={'file_names': file_names},
    dag=dag,
)

mysql_to_csv_complete_task >> email_task
mysql_to_csv_incomplete_task >> email_task
email_task >> csv_cleanup
