import pendulum
from airflow import DAG
from airflow.models import Variable
from airflow.operators.email_operator import EmailOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
from operators.mysql_to_csv import MySqlToCsvFile

local_tz = pendulum.timezone("Asia/Kolkata")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 8, 9, tzinfo=local_tz),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

# 0900 IST
dag = DAG('daily_exclusive', default_args=default_args, schedule_interval='0 9 * * MON-SAT')

now = datetime.now(local_tz)
current_date_message = now.strftime("%d %B, %Y")
current_date_filename = now.strftime("%Y%m%d")
file_names = ["/tmp/only_membership_" + current_date_filename + ".csv",
              "/tmp/all_exclusive_" + current_date_filename + ".csv",
              "/tmp/only_gv_" + current_date_filename + ".csv"]

mysql_to_csv_only_membership_task = MySqlToCsvFile(task_id='only_membership',
                                                   sql="""SELECT o.id as order_id, pm.name, hp.prod_id AS prod_id
                                                          FROM has_products hp
                                                          JOIN orders o on hp.order_id=o.id
                                                          JOIN order_attributes oa ON oa.order_id = o.id
                                                          JOIN payment_method pm ON pm.id = o.payment_method
                                                          WHERE oa.priority_reason = "EXCLUSIVE_USER" AND o.order_status IN (1,21,27) AND o.payment_method!=1
                                                          GROUP BY o.id
                                                          HAVING SUM(hp.qty)=1 AND prod_id IN (SELECT id FROM products WHERE prod_type=5 AND is_avail=1 AND is_del=0);
                                                          """,
                                                   file_name=file_names[0],
                                                   dag=dag)

mysql_to_csv_all_exclusive_task = MySqlToCsvFile(task_id='all_exclusive',
                                                 sql="""SELECT o.id as order_id
                                                        FROM orders o
                                                        JOIN order_attributes oa ON oa.order_id = o.id
                                                        WHERE oa.priority_reason = "EXCLUSIVE_USER" AND o.order_status IN (1,21,27)
                                                        GROUP BY o.id;""",
                                                 file_name=file_names[1],
                                                 dag=dag)

mysql_to_csv_only_gv_task = MySqlToCsvFile(task_id='only_gv',
                                           sql="""SELECT o.id as order_id
                                                    FROM orders o
                                                    JOIN gift_vouchers gv ON gv.order_id = o.id
                                                    WHERE o.order_status IN (1,21,27)
                                                    AND o.payment_method!=1
                                                    AND o.id NOT IN (
                                                        SELECT o1.id
                                                        FROM orders o1
                                                        JOIN has_products hp ON o1.id = hp.order_id
                                                        WHERE o1.order_status IN (1,21,27)
                                                        AND o.payment_method!=1
                                                    )
                                                    GROUP BY o.id;
                                                 """,
                                           file_name=file_names[2],
                                           dag=dag)

date_message = "Date of generation: " + current_date_message + "<br><br>"
body_message = date_message + """
                            Please find attached:<br>
                                1. Only exclusive membership prepaid (order status - placed, processing, order issue)<br>
                                2. All exclusive orders (order status - placed, processing, order issue)
                                3. Only gift vouchers prepaid (order status - placed, processing, order issue)
                            """

email_task = EmailOperator(
    to=Variable.get("daily_exclusive_recipients", deserialize_json=True)["to"],
    cc=Variable.get("daily_exclusive_recipients", deserialize_json=True)["cc"],
    task_id='send_daily_exclusive_email',
    subject='Daily exclusive report',
    html_content=body_message,
    mime_charset='us-ascii',
    files=file_names,
    dag=dag)


def delete_files(file_names):
    import os
    for file in file_names:
        if os.path.exists(file):
            os.remove(file)
            print('File removed: ' + file)
        else:
            print('File doesnt exist: ' + file)


csv_cleanup = PythonOperator(
    task_id='csv_cleanup',
    python_callable=delete_files,
    op_kwargs={'file_names': file_names},
    dag=dag,
)

mysql_to_csv_only_membership_task >> email_task
mysql_to_csv_all_exclusive_task >> email_task
mysql_to_csv_only_gv_task >> email_task
email_task >> csv_cleanup
