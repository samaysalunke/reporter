from datetime import datetime, timedelta

import pendulum
from airflow import DAG
from airflow.models import Variable
from airflow.operators.email_operator import EmailOperator
from airflow.operators.python_operator import PythonOperator
from operators.mysql_to_csv import MySqlToCsvFile

local_tz = pendulum.timezone("Asia/Kolkata")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 11, 25, tzinfo=local_tz),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

# 0900 IST
dag = DAG('active_products_missing_fields', default_args=default_args, schedule_interval='0 9 * * *')

now = datetime.now(local_tz)
current_date_message = now.strftime("%d %B, %Y")
current_date_filename = now.strftime("%Y%m%d")
file_names = ["/tmp/missing_image_" + current_date_filename + ".csv",
              "/tmp/missing_exclusive_price_" + current_date_filename + ".csv",
              "/tmp/missing_cog_" + current_date_filename + ".csv",
              "/tmp/missing_category_" + current_date_filename + ".csv",
              "/tmp/missing_artist_" + current_date_filename + ".csv",
              "/tmp/missing_gtin_barcode_" + current_date_filename + ".csv",
              "/tmp/missing_zone_id_" + current_date_filename + ".csv",
              "/tmp/missing_rack_id_" + current_date_filename + ".csv"
              ]

mysql_to_csv_missing_image_task = MySqlToCsvFile(task_id='missing_image',
                                                 sql="""
                                                        -- active products with empty images
                                                        SELECT
                                                        pap.id AS ParentProductId,
                                                        pap.product AS ParentProduct,
                                                        IFNULL(c.category,"") AS ProductCategory,
                                                        IFNULL(ua.firstname,"") AS ArtistName,
                                                        pap.images AS Images
                                                        FROM products pap
                                                        LEFT JOIN categories AS c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                                        LEFT JOIN users ua ON ua.id = pap.added_by
                                                        WHERE pap.is_avail=1 AND pap.is_del=0 AND pap.parent_prod_id = 0 AND (pap.images="" OR pap.images="[]")
                                                        GROUP BY pap.id
                                                        ORDER BY ProductCategory, ParentProduct;
                                                  """,
                                                 file_name=file_names[0],
                                                 dag=dag)

mysql_to_csv_missing_exclusive_price_task = MySqlToCsvFile(task_id='missing_exclusive_price',
                                                           sql="""
                                                        SELECT
                                                        pap.id AS ParentProductId,
                                                        pap.product AS ParentProduct,
                                                        IFNULL(c.category,"") AS ProductCategory,
                                                        IFNULL(ua.firstname,"") AS ArtistName
                                                        FROM products pap
                                                        LEFT JOIN categories AS c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                                        LEFT JOIN users ua ON ua.id = pap.added_by
                                                        WHERE pap.is_avail=1 AND pap.is_del=0 AND pap.parent_prod_id = 0 
                                                        AND pap.id NOT IN (SELECT product_id FROM pricelist WHERE pricelist_id = 2)
                                                        GROUP BY pap.id
                                                        ORDER BY ProductCategory, ParentProduct;
                                                  """,
                                                           file_name=file_names[1],
                                                           dag=dag)

mysql_to_csv_missing_cog_task = MySqlToCsvFile(task_id='missing_cog',
                                               sql="""
                                                        -- active products with 0 COG
                                                        SELECT
                                                        pap.id AS ParentProductId,
                                                        pap.product AS ParentProduct,
                                                        IFNULL(c.category,"") AS ProductCategory,
                                                        IFNULL(ua.firstname,"") AS ArtistName,
                                                        pap.cog AS CostOfGood
                                                        FROM products pap
                                                        LEFT JOIN categories AS c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                                        LEFT JOIN users ua ON ua.id = pap.added_by
                                                        WHERE pap.is_avail=1 AND pap.is_del=0 AND pap.parent_prod_id = 0 AND pap.cog = 0 AND pap.prod_type IN (1, 3)
                                                        GROUP BY pap.id
                                                        ORDER BY ProductCategory, ParentProduct;
                                                  """,
                                               file_name=file_names[2],
                                               dag=dag)

mysql_to_csv_missing_category_task = MySqlToCsvFile(task_id='missing_category',
                                                    sql="""
                                                    -- active products with no categories assigned
                                                    SELECT
                                                    pap.id AS ParentProductId,
                                                    pap.product AS ParentProduct,
                                                    IFNULL(c.category,"") AS ProductCategory,
                                                    IFNULL(ua.firstname,"") AS ArtistName,
                                                    pap.url_key AS ProductUrlKey
                                                    FROM products pap
                                                    LEFT JOIN categories AS c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                                    LEFT JOIN users ua ON ua.id = pap.added_by
                                                    WHERE pap.is_avail=1 AND pap.is_del=0 AND pap.parent_prod_id = 0 AND c.id IS NULL
                                                    GROUP BY pap.id
                                                    ORDER BY ProductCategory, ParentProduct;
                                                  """,
                                                    file_name=file_names[3],
                                                    dag=dag)

mysql_to_csv_missing_artist_task = MySqlToCsvFile(task_id='missing_artist',
                                                  sql="""
                                                    -- active products with no artist assigned
                                                    SELECT
                                                    pap.id AS ParentProductId,
                                                    pap.product AS ParentProduct,
                                                    IFNULL(c.category,"") AS ProductCategory,
                                                    IFNULL(ua.firstname,"") AS ArtistName
                                                    FROM products pap
                                                    LEFT JOIN categories AS c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                                    LEFT JOIN users ua ON ua.id = pap.added_by
                                                    WHERE pap.is_avail=1 AND pap.is_del=0 AND pap.parent_prod_id = 0 AND ua.id IS NULL
                                                    GROUP BY pap.id
                                                    ORDER BY ProductCategory, ParentProduct;
                                                  """,
                                                  file_name=file_names[4],
                                                  dag=dag)

mysql_to_csv_missing_barcode_task = MySqlToCsvFile(task_id='missing_barcode',
                                                   sql="""
                                                    -- active products with no barcode assigned
                                                    SELECT
                                                    p.id AS ProductId,
                                                    pap.product AS ParentProduct,
                                                    IFNULL(ho.attr_val,"") AS Size,
                                                    IFNULL(c.category,"") AS ProductCategory,
                                                    IFNULL(ua.firstname,"") AS ArtistName
                                                    FROM products p
                                                    JOIN products pap ON pap.id = CASE WHEN p.parent_prod_id >0 THEN p.parent_prod_id ELSE p.id END 
                                                    LEFT JOIN categories AS c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                                    LEFT JOIN users ua ON ua.id = pap.added_by
                                                    LEFT JOIN has_options ho ON ho.prod_id = p.id
                                                    WHERE pap.is_avail=1 AND pap.is_del=0 AND pap.parent_prod_id = 0 AND p.prod_type = 1 AND p.is_del=0 AND p.id NOT IN (SELECT product_id FROM products_gs1)
                                                    GROUP BY p.id
                                                    ORDER BY ProductCategory, ParentProduct;
                                                  """,
                                                   file_name=file_names[5],
                                                   dag=dag)

mysql_to_csv_missing_zone_id_task = MySqlToCsvFile(task_id='missing_zone_id',
                                                   sql="""
                                                    SELECT
                                                    p.id AS ProductId,
                                                    pap.product AS ParentProduct,
                                                    IFNULL(ho.attr_val,"") AS Size,
                                                    IFNULL(c.category,"") AS ProductCategory,
                                                    IFNULL(ua.firstname,"") AS ArtistName
                                                    FROM products p
                                                    JOIN products pap ON pap.id = CASE WHEN p.parent_prod_id >0 THEN p.parent_prod_id ELSE p.id END 
                                                    LEFT JOIN categories AS c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                                    LEFT JOIN users ua ON ua.id = pap.added_by
                                                    LEFT JOIN has_options ho ON ho.prod_id = p.id
                                                    WHERE pap.is_avail=1 AND pap.is_del=0 AND pap.parent_prod_id = 0 AND p.prod_type = 1 AND p.is_del=0 AND p.id NOT IN (SELECT product_id FROM products_warehouses)
                                                    GROUP BY p.id
                                                    ORDER BY ProductCategory, ParentProduct;
                                                  """,
                                                   file_name=file_names[6],
                                                   dag=dag)

mysql_to_csv_missing_rack_id_task = MySqlToCsvFile(task_id='missing_rack_id',
                                                   sql="""
                                                    SELECT
                                                    p.id AS ProductId,
                                                    pap.product AS ParentProduct,
                                                    IFNULL(ho.attr_val,"") AS Size,
                                                    IFNULL(c.category,"") AS ProductCategory,
                                                    IFNULL(ua.firstname,"") AS ArtistName
                                                    FROM products p
                                                    JOIN products pap ON pap.id = CASE WHEN p.parent_prod_id >0 THEN p.parent_prod_id ELSE p.id END 
                                                    LEFT JOIN categories AS c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                                    LEFT JOIN users ua ON ua.id = pap.added_by
                                                    LEFT JOIN has_options ho ON ho.prod_id = p.id
                                                    WHERE pap.is_avail=1 AND pap.is_del=0 AND pap.parent_prod_id = 0 AND p.prod_type = 1 AND p.is_del=0
                                                    AND p.warehouse_code="" AND p.is_jit!=1
                                                    GROUP BY p.id
                                                    ORDER BY ProductCategory, ParentProduct;
                                                  """,
                                                   file_name=file_names[7],
                                                   dag=dag)

date_message = "Date of generation: " + current_date_message + "<br><br>"
body_message = date_message + """
                            Please find attached:<br>
                                1. Active products missing image<br>
                                2. Active products missing exclusive price<br>
                                3. Active products missing COG<br>
                                4. Active products missing category<br>
                                5. Active products missing artist<br>
                                6. Active products missing GTIN barcode <br>
                                7. Active products missing zone id<br>
                                8. Active products missing rack id<br>
                            """

email_task = EmailOperator(
    to=Variable.get("daily_active_products_missing_fields_recipients", deserialize_json=True)["to"],
    cc=Variable.get("daily_active_products_missing_fields_recipients", deserialize_json=True)["cc"],
    task_id='send_daily_active_products_missing_fields_email',
    subject='Active products missing fields',
    html_content=body_message,
    mime_charset='us-ascii',
    files=file_names,
    dag=dag)


def delete_files(file_names):
    import os
    for file in file_names:
        if os.path.exists(file):
            os.remove(file)
            print('File removed: ' + file)
        else:
            print('File doesnt exist: ' + file)


csv_cleanup = PythonOperator(
    task_id='csv_cleanup',
    python_callable=delete_files,
    op_kwargs={'file_names': file_names},
    dag=dag,
)

mysql_to_csv_missing_image_task >> email_task
mysql_to_csv_missing_exclusive_price_task >> email_task
mysql_to_csv_missing_cog_task >> email_task
mysql_to_csv_missing_category_task >> email_task
mysql_to_csv_missing_artist_task >> email_task
mysql_to_csv_missing_barcode_task >> email_task
mysql_to_csv_missing_zone_id_task >> email_task
mysql_to_csv_missing_rack_id_task >> email_task
email_task >> csv_cleanup
