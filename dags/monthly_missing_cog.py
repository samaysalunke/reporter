from datetime import datetime, timedelta

import pendulum
from airflow import DAG
from airflow.operators.email_operator import EmailOperator
from airflow.operators.python_operator import PythonOperator
from operators.mysql_to_csv import MySqlToCsvFile
from airflow.utils import dates

local_tz = pendulum.timezone("Asia/Kolkata")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': dates.days_ago(0),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

# 1000 IST
dag = DAG('monthly_missing_cog', default_args=default_args, schedule_interval='30 4 10 * *')

now = datetime.now(local_tz)
current_date_message = now.strftime("%d %B, %Y")
current_date_filename = now.strftime("%Y%m%d")
file_names = ["/tmp/missing_cog_" + current_date_filename + ".csv"]

mysql_to_csv_missing_cog_task = MySqlToCsvFile(task_id='missing_cog',
                                            sql="""
                                            SELECT
                                            pap.id AS ParentProductId,
                                            pap.product AS ParentProduct,
                                            c.category AS ProductCategory,
                                            pap.cog AS CostOfGood,
                                            ua.firstname AS ArtistName
                                            FROM has_products hp
                                            JOIN orders o ON o.id = hp.order_id
                                            LEFT JOIN order_attributes oa ON o.id = oa.order_id
                                            LEFT JOIN users u ON o.user_id = u.id
                                            JOIN products p ON p.id = (CASE WHEN hp.sub_prod_id>0 THEN hp.sub_prod_id ELSE hp.prod_id END)
                                            JOIN products pap ON pap.id = (CASE WHEN p.parent_prod_id>0 THEN p.parent_prod_id ELSE p.id END)
                                            LEFT JOIN categories AS c ON (c.id = (SELECT cat_id FROM has_categories WHERE prod_id = pap.id AND cat_id NOT IN (162,148) ORDER BY CASE WHEN cat_id=153 THEN FIELD(153, cat_id) ELSE id END LIMIT 1))
                                            LEFT JOIN users ua ON ua.id = pap.added_by
                                            LEFT JOIN order_status os ON o.order_status = os.id
                                            LEFT JOIN payment_method pm ON o.payment_method = pm.id
                                            LEFT JOIN payment_status ps ON o.payment_status = ps.id
                                            LEFT JOIN courier_companies cc ON o.courier = cc.id
                                            WHERE o.order_status NOT IN (0,17)
                                            AND YEAR(o.created_at) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH)
                                            AND MONTH(o.created_at) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)
                                            AND pap.cog=0
                                            AND pap.id NOT IN (SELECT id FROM products WHERE prod_type=4 AND is_avail=1 AND is_del=0)
                                            GROUP BY pap.id
                                            ORDER BY c.category, p.product ASC;
                                            """,
                                            file_name=file_names[0],
                                            dag=dag)


date_message = "Date of generation: " + current_date_message + "<br><br>"
month_year_date = (now - timedelta(30)).strftime("%B, %Y")

body_message = date_message + """
                            Please find attached:<br>
                                1. Products missing COG for """ + month_year_date + """ sales
                            """

email_task = EmailOperator(
    to=['rushabh.shah@thesouledstore.com', 'jugal.kansara@thesouledstore.com'],
    cc=['tech@thesouledstore.com'],
    task_id='send_montly_missing_cog_email',
    subject='[Alert] Products missing COG for '+ month_year_date+' sales',
    html_content=body_message,
    mime_charset='us-ascii',
    files=file_names,
    dag=dag)


def delete_files(file_names):
    import os
    for file in file_names:
        if os.path.exists(file):
            os.remove(file)
            print('File removed: ' + file)
        else:
            print('File doesnt exist: ' + file)


csv_cleanup = PythonOperator(
    task_id='csv_cleanup',
    python_callable=delete_files,
    op_kwargs={'file_names': file_names},
    dag=dag,
)

mysql_to_csv_missing_cog_task >> email_task
email_task >> csv_cleanup
