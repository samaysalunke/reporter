import pendulum
from airflow import DAG
from airflow.models import Variable
from airflow.operators.email_operator import EmailOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
from operators.mysql_to_csv import MySqlToCsvFile

local_tz = pendulum.timezone("Asia/Kolkata")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 12, 16, tzinfo=local_tz),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

# 0900 IST
dag = DAG('daily_chennai_orders_14_30', default_args=default_args, schedule_interval='30 14 * * *')

now = datetime.now(local_tz)
current_date_message = now.strftime("%d %B, %Y")
current_date_filename = now.strftime("%Y%m%d")
file_names = ["/tmp/chennai_orders_14_30_" + current_date_filename + ".csv"]

mysql_to_csv_chennai_orders_task = MySqlToCsvFile(task_id='mysql_to_csv_chennai_orders_task',
                                                  sql="""
                                                        SELECT o.id AS id,
                                                            CONCAT(u.firstname, " ", u.lastname) AS accountName, u.email,
                                                            CONCAT(o.first_name," ", o.last_name) as recipientName, CONCAT(IFNULL(o.address1,""), ", ", IFNULL(o.address2,""), ", ", IFNULL(o.address3,"")) AS recipientAddress,
                                                            o.city as city, o.zone_id as state, o.country_id as country, o.postal_code AS pincode, o.phone_no,
                                                            IFNULL(pm.name,"") AS payment_method, IFNULL(ps.payment_status,"") AS payment_status, os.order_status,
                                                            o.order_amt AS order_amt, o.cod_charges AS cod_charges, o.gifting_charges, o.cashback_used AS cashback_used,
                                                            o.shipping_amt AS shipping_price,
                                                            SUM(hp.disc) AS coupon_discount, IFNULL(c.voucher_code,"") AS coupon_code, o.voucher_amt_used AS voucher_discount, IFNULL(v.voucher_code, "") AS voucher_code,
                                                            ROUND(SUM((hp.price-hp.disc)*hp.gst/100),2) AS gst, o.pay_amt AS pay_amt,
                                                            DATE(o.created_at) AS order_date, cc.name AS courierService, o.shiplabel_tracking_id as trackingId, IFNULL(f.flag_name,"") AS flag_name
                                                        FROM orders o
                                                        JOIN has_products hp ON hp.order_id = o.id
                                                        JOIN users u ON o.user_id = u.id
                                                        JOIN order_status os ON o.order_status = os.id
                                                        LEFT JOIN payment_method pm ON o.payment_method = pm.id
                                                        LEFT JOIN courier_companies cc ON cc.id = o.courier
                                                        LEFT JOIN payment_status ps ON ps.id = o.payment_status
                                                        LEFT JOIN coupons c ON c.id = o.coupon_used
                                                        LEFT JOIN coupons v ON v.id = o.voucher_used
                                                        LEFT JOIN flags f ON f.id = o.flag_id
                                                        WHERE o.order_status IN (1,21,27) 
                                                        AND o.postal_code IN (799005,799006,799007,799008,799009,799010,799012,799013,799014,799035,799102,799103,799105,799114,799115,799120,799130,799142,799144,799155,799201,799202,799203,799205,799207,799210,799003,799001,799002,799004,799046,799022,791001,791102,791125,792001,792056,792103,792110,792120,792121,792123,792105,792130,785102,785104,785105,785106,785110,785662,785670,785673,785675,785676,786004,786005,786007,786008,786010,786012,786101,786102,786103,786125,786126,786145,786146,786147,786150,786151,786152,786153,786155,786156,786157,786158,786159,786160,786170,786171,786173,786174,786181,786182,786183,786184,786187,786188,786189,786190,786191,786192,786602,786610,786611,786612,786613,786621,786622,786623,786692,787026,787029,787034,787053,787055,787056,787057,787058,787059,787060,786001,786002,786003,786006,785669,786172,786124,786186,786185,781023,781030,781101,781102,781103,781104,781121,781123,781124,781127,781130,781132,781134,781137,781141,781150,781303,781304,781310,781334,781335,781337,781339,781340,781341,781344,781346,781347,781348,781349,781351,781353,781354,781365,781366,781367,781369,781370,781376,781380,781381,781382,782103,782104,782105,782108,782401,782402,782403,782410,782411,782412,782413,782425,782426,782427,782429,782435,782439,782440,782441,782442,782445,782446,782447,782460,782462,782481,782482,782485,783101,783120,783121,783122,783123,783124,783125,783126,783127,783129,783130,783131,783132,783135,783330,783382,784115,784116,784125,784145,784146,784148,788819,788830,794104,794105,781128,781013,781014,781015,781017,781031,781035,781039,781033,781001,781012,781011,781010,781009,781171,781027,781026,781034,781025,781019,781008,781016,781024,781021,781020,781004,781003,781018,781125,781037,781022,781005,781007,781032,781036,781006,781028,781029,781038,781040,781122,782106,784529,781002,781375,781068,781320,781336,781135,782448,795001,795003,795004,795005,795008,793010,793016,793018,793101,793102,793105,793150,794001,794101,794111,793004,793007,793006,793005,793003,793002,793001,793022,793014,793008,793017,793011,797001,797103,797112,797113,797115,797116,797117,784151,785001,785004,785005,785006,785007,785008,785009,785010,785013,785014,785015,785101,785107,785111,785112,785601,785602,785603,785609,785610,785611,785612,785613,785614,785615,785616,785618,785620,785621,785622,785625,785626,785630,785633,785634,785635,785636,785640,785663,785664,785665,785666,785667,785672,785674,785680,785681,785682,785683,785684,785685,785686,785687,785688,785689,785690,785691,785692,785694,785695,785696,785697,785698,785699,785700,785701,785702,785703,785704,785660,785003,785002,785623,785627,785012,785011,785016,785018,785113,786121,785678,785641,785638,785648,785649,785645,785643,785644,785647,785654,785656,785668,785631,798601,788001,788002,788003,788004,788005,788006,788007,788009,788010,788011,788013,788014,788015,788025,788026,788030,788031,788098,788101,788102,788103,788104,788106,788107,788109,788110,788111,788115,788116,788117,788118,788119,788121,788123,788126,788150,788151,788152,788155,788156,788160,788161,788163,788164,788167,788168,788701,788702,788706,788709,788710,788711,788712,788713,788719,788720,788722,788723,788724,788725,788726,788727,788728,788801,788802,788803,788805,788806,788815,788817,788169,788125,796001,796005,796007,796009,796012,796081,796181,796261,796310,796321,796571,796701,796891,796901,796082,796008,796186,799204,799250,799251,799253,799254,799260,799263,799264,799277,799278,799279,799280,799288,799289,799290,799266,790114,791109,791110,791111,791113,791120,782001,782002,782003,782101,782102,782120,782122,782123,782125,782126,782135,782136,782137,782138,782139,782140,782141,782142,782143,782144,784001,784025,784026,784027,784028,784101,784102,784103,784104,784105,784110,784118,784149,784150,784153,784154,784164,784165,784167,784168,784174,784175,784176,784177,784178,784180,784182,784184,784189,784501,784505,784506,787001,787031,787032,787051,787052,784010,784161,784002,784004,784003,782010)
                                                        GROUP BY o.id
                                                        ORDER BY o.id;
                                                    """,
                                                  file_name=file_names[0],
                                                  dag=dag)

date_message = "Date of generation: " + current_date_message + "<br><br>"
yesterday = (now - timedelta(1)).strftime("%d %B, %Y")
body_message = date_message + """
                            Please find attached:<br>
                                1. Orders from specified pincodes (order status - placed, processing, order issue)<br>
                            """

email_task = EmailOperator(
    to=Variable.get("daily_chennai_orders_recipients", deserialize_json=True)["to"],
    cc=Variable.get("daily_chennai_orders_recipients", deserialize_json=True)["cc"],
    task_id='send_daily_exclusive_email',
    subject="Daily pincode wise order export",
    html_content=body_message,
    mime_charset='us-ascii',
    files=file_names,
    dag=dag)


def delete_files(file_names):
    import os
    for file in file_names:
        if os.path.exists(file):
            os.remove(file)
            print('File removed: ' + file)
        else:
            print('File doesnt exist: ' + file)


csv_cleanup = PythonOperator(
    task_id='csv_cleanup',
    python_callable=delete_files,
    op_kwargs={'file_names': file_names},
    dag=dag,
)

mysql_to_csv_chennai_orders_task >> email_task >> csv_cleanup
