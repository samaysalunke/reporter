# -*- coding: utf-8 -*-
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.


from airflow.hooks.mysql_hook import MySqlHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
import csv


class MySqlToCsvFile(BaseOperator):

    @apply_defaults
    def __init__(
            self,
            sql,
            delimiter=',',
            mysql_conn_id='tss_sldb',
            file_name='',
            skip_header=False,
            *args, **kwargs):
        super(MySqlToCsvFile, self).__init__(*args, **kwargs)
        self.sql = sql
        self.delimiter = str(delimiter)
        self.mysql_conn_id = mysql_conn_id
        self.file_name = file_name
        self.skip_header = skip_header

    def execute(self, context):
        mysql = MySqlHook(mysql_conn_id=self.mysql_conn_id)
        self.log.info("Dumping MySQL query results to local file")
        conn = mysql.get_conn()
        cursor = conn.cursor()
        self.log.info("Executing: ", self.sql)
        cursor.execute(self.sql)
        with open(self.file_name, "w") as f:
            csv_writer = csv.writer(f, delimiter=self.delimiter)
            if not self.skip_header:
                headers = [item[0] for item in cursor.description]
                csv_writer.writerow(headers)
            for item in cursor.fetchall():
                csv_writer.writerow(item)
            f.flush()
            cursor.close()
            conn.close()
