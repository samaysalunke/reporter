import json
import re

siteUrl = 'https://www.thesouledstore.com'
productUrl = siteUrl + '/product/'
categoryUrl = siteUrl + '/explore/'
artistUrl = siteUrl + '/artists/'
productImageUrl = 'https://images.thesouledstore.com/public/theSoul/uploads/catalog/product/'
title = 'The Souled Store'
age_group = 'adult'
shippingP = "Estimated Delivery Time:<br>Metros: 1-4 days<br>Rest of India: 4-7 days"
conditionP = 'new'


# First image
def getImage(imageStr):
    if not imageStr:
        return ''
    imageList = json.loads(imageStr)
    if len(imageList) < 1:
        return ''
    else:
        return productImageUrl + imageList[0]


# Second image
def getImage1(imageStr):
    if not imageStr:
        return ''
    imageList = json.loads(imageStr)
    if len(imageList) == 0 or len(imageList) == 1:
        return ''
    else:
        return productImageUrl + imageList[1]


# remove tags
def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    cleanr2 = re.compile('nbsp')
    cleantext = re.sub(cleanr2, ' ', cleantext)
    cleanr3 = re.compile('&')
    cleantext = re.sub(cleanr3, '', cleantext)
    cleanr4 = re.compile(';')
    cleantext = re.sub(cleanr4, '', cleantext)
    cleanr5 = re.compile('Â')
    cleantext = re.sub(cleanr5, '', cleantext)
    return cleantext


def pluralToSingular(s):
    if s.endswith("s") and not (s.endswith("Dress") or s.endswith("Boxers") or s.endswith("Socks")):
        return s[:-1]
    else:
        return s


def removeDuplicateCategory(s):
    remainingAndLast = s.rsplit(' ', 1)
    if len(remainingAndLast) == 2:
        remainingTitle = remainingAndLast[0]
        lastWord = remainingAndLast[1]
        remainingAndLast1 = remainingTitle.rsplit(' ', 1)
        if len(remainingAndLast1) == 2:
            secondlastWord = remainingAndLast1[1]
            if lastWord.lower() == secondlastWord.lower():
                return remainingTitle
    return s


def justColor(s):
    return s.split(' (')[0]


# add a new line
def newline(strline):
    return strline.replace('<br>', '\n')


def striptags(s):
    return (s.replace('<', '')).replace('>', '').replace('"', '')


def changeSizeFormat(s):
    s = s.replace(",", "/")
    s = s.replace("XXXXL", "4XL")
    s = s.replace("XXXL", "3XL")
    s = s.replace("XXL", "2XL")
    return s


def createNode(doc, entry, key, value):
    keyNode = doc.createElement("g:" + key)
    valueNode = doc.createTextNode(value)
    keyNode.appendChild(valueNode)
    entry.appendChild(keyNode)


def createSitemapNode(doc, entry, key, value):
    keyNode = doc.createElement(key)
    valueNode = doc.createTextNode(value)
    keyNode.appendChild(valueNode)
    entry.appendChild(keyNode)


def sentanceCase(s):
    if s.isupper():
        return s.capitalize()
    return s
