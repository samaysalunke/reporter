import sys

sys.path.append('/usr/local/airflow/dags/')

from operators.get_mysql_cursor import MySqlCursor
from collections import defaultdict
import json
import time
import traceback
from pprint import pprint


class SyncProduct:
    def __init__(self, session):
        self.session = session
        self.time_taken = time.time()
        self.product_tags = {}
        self.product_variants = {}
        self.product_pricelists = {}
        self.exclusive_pricelist = {}
        self.product_categories = {}

    def print_time_information(self):
        print('Time Elapsed:{} minutes'.format(self.time_taken / 60))

    def get_products(self):
        query = """
                    SELECT
                    p.id as product_id, p.product, p.url_key AS product_url,
                    p.price, p.spl_price,
                    CASE
                      WHEN p.prod_type = 3 AND CAST(IFNULL((SELECT SUM(subp.stock) FROM products subp WHERE subp.parent_prod_id = p.id AND subp.stock>0 AND subp.is_del = 0 GROUP BY p.id),0) AS UNSIGNED) > 0 THEN "True"
                      WHEN p.prod_type = 3 AND CAST(IFNULL((SELECT SUM(subp.stock) FROM products subp WHERE subp.parent_prod_id = p.id AND subp.stock>0 AND subp.is_del = 0 GROUP BY p.id),0) AS UNSIGNED) < 1 THEN "False"
                      WHEN p.prod_type != 3 AND p.stock > 0 THEN "True"
                      WHEN p.prod_type != 3 AND p.stock <= 0 THEN "False"
                    END AS in_stock,
                    CASE WHEN p.is_avail=1 AND p.is_del=0 THEN "True" ELSE "False" END AS is_active,
                    p.images,
                    CASE
                      WHEN p.prod_type=1 THEN "Simple"
                      WHEN p.prod_type=2 THEN "Combo"
                      WHEN p.prod_type=3 THEN "Configurable"
                      WHEN p.prod_type=4 THEN "Freebie"
                      WHEN p.prod_type=5 THEN "Exclusive membership"
                    END AS product_type,
                    p.short_desc AS product_description,
                    IFNULL(ad.desc, "") AS product_details,
                    IFNULL(ua.firstname, "") AS artist,
                    IFNULL(ua.desc, "") AS artist_details,
                    p.meta_title, p.meta_desc, p.meta_keys,
                    p.target_date AS release_date
                    FROM products p
                    LEFT JOIN add_desc ad ON ad.id = p.add_desc_id
                    LEFT JOIN users ua ON ua.id = p.added_by
                    WHERE p.parent_prod_id = 0
                    GROUP BY p.id;
                """

        query_parameters = {}
        self.session.execute(query, query_parameters)
        items = self.session.fetchall()
        products = []
        for product in items:
            product_id = product['product_id']
            product['price'] = int(product['price'])
            product['spl_price'] = int(product['spl_price'])
            product['images'] = json.loads(product['images']) if product['images'] else []
            product['release_date'] = str(product['release_date'])
            product['meta_data'] = {
                'meta_description': product.pop('meta_desc'),
                'meta_keys': product.pop('meta_keys'),
                'meta_title': product.pop('meta_title')
            }

            product['artist'] = product['artist']

            # multiple categories mapping
            product_categories = self.product_categories.get(product_id, [])
            product['categories'] = [x['category'] for x in product_categories]

            # multiple tags mapping
            product_tags = self.product_tags.get(product_id, [])
            for product_tag in product_tags:
                del product_tag['product_key']
            product['genres'] = product_tags

            # multiple pricelists mapping
            product_pricelists = self.product_pricelists.get(product_id, [])
            for product_pricelist in product_pricelists:
                del product_pricelist['product_id']
            product['pricelists'] = product_pricelists

            # exclusive pricelist mapping
            exclusive_pricelist = self.exclusive_pricelist.get(product_id, {})
            product['exclusive_pricelist'] = exclusive_pricelist

            # multiple variants mapping
            product_variants = self.product_variants.get(product_id, [])
            for product_variant in product_variants:
                del product_variant['parent_prod_id']
            product['variants'] = product_variants
            product['in_stock'] = True if product['in_stock'] == "True" else False
            product['is_active'] = True if product['is_active'] == "True" else False
            product['flags'] = [
                {
                    'name': 'Souled out',
                    'color': '#FF0000',
                    'priority': 1,
                    'is_active': True,
                    'value': True if product['in_stock'] == "False" else False

                },
                {
                    'name': 'Limited edition',
                    'color': '#40E0D0',
                    'priority': 2,
                    'is_active': False,
                    'value': False
                },
                {
                    'name': 'Fast selling',
                    'color': '#40E0D0',
                    'priority': 3,
                    'is_active': False,
                    'value': False
                },
                {
                    'name': 'Trending',
                    'color': '#40E0D0',
                    'priority': 4,
                    'is_active': False,
                    'value': False
                },
                {
                    'name': 'New',
                    'color': '#40E0D0',
                    'priority': 5,
                    'is_active': False,
                    'value': False
                }
            ]
            products.append(product)

        return products

    def make_product_categories(self):
        query = """
                SELECT hc.prod_id, c.category
                 FROM has_categories hc
                 JOIN categories c ON hc.cat_id = c.id
                 WHERE cat_id NOT IN (162, 148)
                 ORDER BY hc.id;
            """
        query_parameters = {}
        self.session.execute(query, query_parameters)
        categories = self.session.fetchall()
        data = defaultdict(lambda: [])
        for row in categories:
            data[int(row['prod_id'])].append(row)
        self.product_categories = data

    def make_product_tags(self):
        query = """
                SELECT
                 taggable_id AS product_key,
                 tag_slug,
                 tag_name
                 FROM tagging_tagged
                 WHERE taggable_type = 'Product'
            """
        query_parameters = {}
        self.session.execute(query, query_parameters)
        tags = self.session.fetchall()
        data = defaultdict(lambda: [])
        for row in tags:
            data[int(row['product_key'])].append(row)
        self.product_tags = data

    def make_product_variants(self):
        query = """
                SELECT
                  p.parent_prod_id,
                  p.id as product_id,
                  ho.attr_val AS size,
                  CASE WHEN p.stock>0 THEN "True" ELSE "False" END AS in_stock,
                  CASE WHEN p.is_del=0 THEN "True" ELSE "False" END AS is_active
                FROM products p
                JOIN has_options ho ON ho.prod_id = p.id
                WHERE p.parent_prod_id>0
                GROUP BY p.id
                """
        query_parameters = {}
        self.session.execute(query, query_parameters)
        variants = self.session.fetchall()
        data = defaultdict(lambda: [])
        for row in variants:
            row['in_stock'] = True if row['in_stock'] == "True" else False
            row['is_active'] = True if row['is_active'] == "True" else False
            data[row['parent_prod_id']].append(row)
        self.product_variants = data

    def make_product_pricelists(self):
        query = """
                    SELECT p.product_id, p.pricelist_id, p.price, pm.start_date, pm.expiry_date,
                    pm.priority, pm.is_active, pm.is_special_price, pr.price AS original_price
                    FROM pricelist p
                    JOIN products pr ON p.product_id = pr.id
                    JOIN pricelist_meta pm on p.pricelist_id = pm.id
                    """
        query_parameters = {}
        self.session.execute(query, query_parameters)
        pricelists = self.session.fetchall()
        data = defaultdict(lambda: [])
        exclusive_data = defaultdict(lambda: {})
        for row in pricelists:
            row['is_active'] = True if row['is_active'] == 1 else False
            row['is_special_price'] = True if row['is_special_price'] == 1 else False
            row['original_price'] = int(row['original_price'])
            row['start_date'] = str(row['start_date'])
            row['expiry_date'] = str(row['expiry_date'])
            data[row['product_id']].append(row)
            if row['pricelist_id'] == 2:
                exclusive_data[row['product_id']] = row
        self.exclusive_pricelist = exclusive_data
        self.product_pricelists = data

    def insert_into_file(self, json_object):
        import os
        filename = '/tmp/wYMqdgFsYETkw7P6.json'
        with open(filename, 'w') as outfile:
            json.dump(json_object, outfile)


if __name__ == '__main__':
    print('Building products for unbxd')
    mysql_cursor = MySqlCursor(task_id='run_feed')
    cursor = mysql_cursor.get_cursor()
    sync_product = SyncProduct(cursor)
    sync_product.make_product_categories()
    sync_product.make_product_tags()
    sync_product.make_product_variants()
    sync_product.make_product_pricelists()
    p = sync_product.get_products()
    sync_product.insert_into_file(p)
    sync_product.time_taken = time.time() - sync_product.time_taken
    sync_product.print_time_information()
