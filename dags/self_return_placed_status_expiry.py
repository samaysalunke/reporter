import json
import pendulum
from airflow import DAG
from airflow.operators.email_operator import EmailOperator
from airflow.operators.mysql_operator import MySqlOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
from operators.custom_http_operator import CustomSimpleHttpOperator
from operators.mysql_to_csv import MySqlToCsvFile
from airflow.models import Variable

local_tz = pendulum.timezone("Asia/Kolkata")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 9, 3, tzinfo=local_tz),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

#1600 IST
dag = DAG('self_return_placed_status_expiry', default_args=default_args, schedule_interval='0 16 * * *')

now = datetime.now(local_tz)
current_date_message = now.strftime("%d %B, %Y")
current_date_filename = now.strftime("%Y%m%d")
file_names = ["/tmp/self_return_placed_status_expiry_" + current_date_filename + ".csv"]

mysql_to_csv_self_return_placed_status_expiry_task = MySqlToCsvFile(task_id='self_return_placed_status_expiry',
                                                    sql="""
                                                    SELECT o.id
                                                    FROM orders o
                                                    WHERE o.order_status = 34
                                                    AND o.updated_at < DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 15 DAY);
                                                    """,
                                                    file_name=file_names[0],
                                                    skip_header=True,
                                                    mysql_conn_id="tss_live",
                                                    dag=dag)


update_orders_task = MySqlOperator(task_id='update_orders_task',
                                     sql="""
                                     UPDATE orders
                                     SET order_status = 3, updated_at = NOW()
                                     WHERE order_status = 34
                                     AND updated_at < DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 15 DAY);
                                     """,
                                     mysql_conn_id="tss_live",
                                     autocommit=True,
                                     dag=dag)

date_message = "Date of generation: " + current_date_message + "<br><br>"
body_message = date_message + """
                            Please find attached:<br>
                                1. Self Return Status Greater Than 15 Days Orders
                            """

email_task = EmailOperator(
    to=Variable.get("self_return_placed_status_expiry_recipients", deserialize_json=True)["to"],
    cc=Variable.get("self_return_placed_status_expiry_recipients", deserialize_json=True)["cc"],
    task_id='send_daily_self_return_placed_status_expiry_email',
    subject='Daily self return status expiry report',
    html_content=body_message,
    mime_charset='us-ascii',
    files=file_names,
    dag=dag)

def delete_files(file_names):
    import os
    for file in file_names:
        if os.path.exists(file):
            os.remove(file)
            print('File removed: ' + file)
        else:
            print('File doesnt exist: ' + file)

csv_cleanup = PythonOperator(
    task_id='csv_cleanup',
    python_callable=delete_files,
    op_kwargs={'file_names': file_names},
    dag=dag,
)


mysql_to_csv_self_return_placed_status_expiry_task >> update_orders_task >> email_task >> csv_cleanup
