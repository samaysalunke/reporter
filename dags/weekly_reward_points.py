import pendulum
from airflow import DAG
from airflow.models import Variable
from airflow.operators.email_operator import EmailOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
from operators.mysql_to_csv import MySqlToCsvFile

local_tz = pendulum.timezone("Asia/Kolkata")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 8, 25, tzinfo=local_tz),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

# 1000 IST
dag = DAG('weekly_reward_points', default_args=default_args, schedule_interval='0 10 * * MON')

now = datetime.now(local_tz)
current_date_message = now.strftime("%d %B, %Y")
current_date_filename = now.strftime("%Y%m%d")
file_names = ["/tmp/user_reward_points_0_600_" + current_date_filename + ".csv",
              "/tmp/user_reward_points_600_1200_" + current_date_filename + ".csv",
              "/tmp/user_reward_points_1200_2000_" + current_date_filename + ".csv",
              "/tmp/user_reward_points_above_2000_" + current_date_filename + ".csv"]

mysql_to_csv_user_reward_points_0_600_task = MySqlToCsvFile(task_id='user_reward_points_1',
                                                            sql="""
                                                                SELECT u.id, u.cashback,
                                                                COUNT(o.id) AS order_count,
                                                                SUM(o.pay_amt) AS order_total,
                                                                (SELECT DATE(o1.created_at) FROM orders o1 WHERE o1.user_id = u.id ORDER BY o1.id DESC LIMIT 1) AS last_order_date
                                                                FROM users u
                                                                JOIN orders o ON u.id = o.user_id AND o.order_status NOT IN (0,17)
                                                                WHERE u.cashback BETWEEN 1 and 600
                                                                GROUP BY u.id;
                                                            """,
                                                            file_name=file_names[0],
                                                            dag=dag)

mysql_to_csv_user_reward_points_600_1200_task = MySqlToCsvFile(task_id='user_reward_points_2',
                                                               sql="""
                                                                SELECT u.id, u.cashback,
                                                                COUNT(o.id) AS order_count,
                                                                SUM(o.pay_amt) AS order_total,
                                                                (SELECT DATE(o1.created_at) FROM orders o1 WHERE o1.user_id = u.id ORDER BY o1.id DESC LIMIT 1) AS last_order_date
                                                                FROM users u
                                                                JOIN orders o ON u.id = o.user_id AND o.order_status NOT IN (0,17)
                                                                WHERE u.cashback BETWEEN 601 and 1200
                                                                GROUP BY u.id;
                                                                """,
                                                               file_name=file_names[1],
                                                               dag=dag)

mysql_to_csv_user_reward_points_1200_2000_task = MySqlToCsvFile(task_id='user_reward_points_3',
                                                                sql="""
                                                                SELECT u.id, u.cashback,
                                                                COUNT(o.id) AS order_count,
                                                                SUM(o.pay_amt) AS order_total,
                                                                (SELECT DATE(o1.created_at) FROM orders o1 WHERE o1.user_id = u.id ORDER BY o1.id DESC LIMIT 1) AS last_order_date
                                                                FROM users u
                                                                JOIN orders o ON u.id = o.user_id AND o.order_status NOT IN (0,17)
                                                                WHERE u.cashback BETWEEN 1201 AND 2000
                                                                GROUP BY u.id;
                                                                """,
                                                                file_name=file_names[2],
                                                                dag=dag)
mysql_to_csv_user_reward_points_above_2000_task = MySqlToCsvFile(task_id='user_reward_points_4',
                                                                 sql="""
                                                                    SELECT u.id, u.cashback,
                                                                    COUNT(o.id) AS order_count,
                                                                    SUM(o.pay_amt) AS order_total,
                                                                    (SELECT DATE(o1.created_at) FROM orders o1 WHERE o1.user_id = u.id ORDER BY o1.id DESC LIMIT 1) AS last_order_date
                                                                    FROM users u
                                                                    JOIN orders o ON u.id = o.user_id AND o.order_status NOT IN (0,17)
                                                                    WHERE u.cashback > 2000
                                                                    GROUP BY u.id;
                                                                """,
                                                                 file_name=file_names[3],
                                                                 dag=dag)

date_message = "Date of generation: " + current_date_message + "<br><br>"

body_message = date_message + """
                            Please find attached:<br><br>
                                User reward points data:<br>
                                1. 1 to 600<br>
                                2. 601 to 1200<br>
                                3. 1201 to 2000<br>
                                4. Greater than 2000<br>
                            """

email_task = EmailOperator(
    to=Variable.get("weekly_user_reward_points_recipients", deserialize_json=True)["to"],
    cc=Variable.get("weekly_user_reward_points_recipients", deserialize_json=True)["cc"],
    task_id='send_daily_user_reward_points_email',
    subject='Weekly user reward points report',
    html_content=body_message,
    mime_charset='us-ascii',
    files=file_names,
    dag=dag)


def delete_files(file_names):
    import os
    for file in file_names:
        if os.path.exists(file):
            os.remove(file)
            print('File removed: ' + file)
        else:
            print('File doesnt exist: ' + file)


csv_cleanup = PythonOperator(
    task_id='csv_cleanup',
    python_callable=delete_files,
    op_kwargs={'file_names': file_names},
    dag=dag,
)

mysql_to_csv_user_reward_points_above_2000_task >> email_task
mysql_to_csv_user_reward_points_1200_2000_task >> email_task
mysql_to_csv_user_reward_points_600_1200_task >> email_task
mysql_to_csv_user_reward_points_0_600_task >> email_task
email_task >> csv_cleanup
