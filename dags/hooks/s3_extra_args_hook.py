from airflow.hooks.S3_hook import S3Hook

import boto3


class S3ExtraArgsHook(S3Hook):
    """
    S3Hook: Interact with AWS S3, using the boto3 library.
    S3ObjectACLHook: Update ACL of object inside S3 bucket.
    """

    def get_conn(self):
        return self.get_client_type('s3')

    def load_file_with_extra_args(self,
                  filename,
                  key,
                  bucket_name=None,
                  replace=False,
                  encrypt=False,
                  extra_args={}):
        """
        Loads a local file to S3

        :param filename: name of the file to load.
        :type filename: str
        :param key: S3 key that will point to the file
        :type key: str
        :param bucket_name: Name of the bucket in which to store the file
        :type bucket_name: str
        :param replace: A flag to decide whether or not to overwrite the key
            if it already exists. If replace is False and the key exists, an
            error will be raised.
        :type replace: bool
        :param encrypt: If True, the file will be encrypted on the server-side
            by S3 and will be stored in an encrypted form while at rest in S3.
        :type encrypt: bool
        :param extra_args: Extra arguments that may be passed to the client operation.
        :type extra_args: dict
        """
        if not bucket_name:
            (bucket_name, key) = self.parse_s3_url(key)

        if not replace and self.check_for_key(key, bucket_name):
            raise ValueError("The key {key} already exists.".format(key=key))

        if encrypt:
            extra_args['ServerSideEncryption'] = "AES256"

        client = self.get_conn()
        client.upload_file(filename, bucket_name, key, ExtraArgs=extra_args)
