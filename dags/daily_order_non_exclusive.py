from datetime import datetime, timedelta

import pendulum
from airflow import DAG
from airflow.operators.email_operator import EmailOperator
from airflow.operators.python_operator import PythonOperator
from operators.mysql_to_csv import MySqlToCsvFile
from airflow.models import Variable


local_tz = pendulum.timezone("Asia/Kolkata")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 10, 24, tzinfo=local_tz),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

# 0900 IST
dag = DAG('daily_order_non_exclusive', default_args=default_args, schedule_interval='0 9 * * *')

now = datetime.now(local_tz)
current_date_message = now.strftime("%d %B, %Y")
current_date_filename = now.strftime("%Y%m%d")
file_names = ["/tmp/non_exclusive_" + current_date_filename + ".csv"]

mysql_to_csv_order_export_task = MySqlToCsvFile(task_id='order_export',
                                                sql=""" SELECT DISTINCT u.email
                                                        FROM orders o
                                                        JOIN users u ON o.user_id = u.id
                                                        WHERE o.created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                        AND o.order_status NOT IN (0, 4, 17, 22)
                                                        AND u.id NOT IN (
                                                          SELECT users.id
                                                          FROM user_eav ue
                                                          JOIN users ON ue.entity = users.email
                                                          WHERE ue.is_active = 1 AND ue.attribute="EXCLUSIVE_MEMBER" AND ue.value = 1
                                                        );
                                                          """,
                                                file_name=file_names[0],
                                                dag=dag)


date_message = "Date of generation: " + current_date_message + "<br><br>"
yesterday = (now - timedelta(1)).strftime("%d %B, %Y")
body_message = date_message + """
                            Please find attached:<br>
                            Date range: """ + yesterday + """ 00:00:00 to  """ + yesterday + """ 23:59:59 <br>
                            Email ID for all non exclusive users who have placed orders in the above date range. <br>
                            <a href="mailto:saurabh.shirke@thesouledstore.com">@Saurabh Shirke</a>, please mark them as three month exclusive users.
                            """

email_task = EmailOperator(
    to=Variable.get("daily_order_non_exclusive_recipients", deserialize_json=True)["to"],
    cc=Variable.get("daily_order_non_exclusive_recipients", deserialize_json=True)["cc"],
    task_id='send_daily_order_export_email',
    subject='Daily non exclusive email',
    html_content=body_message,
    mime_charset='us-ascii',
    files=file_names,
    dag=dag)


def delete_files(file_names):
    import os
    for file in file_names:
        if os.path.exists(file):
            os.remove(file)
            print('File removed: ' + file)
        else:
            print('File doesnt exist: ' + file)


csv_cleanup = PythonOperator(
    task_id='csv_cleanup',
    python_callable=delete_files,
    op_kwargs={'file_names': file_names},
    dag=dag,
)

mysql_to_csv_order_export_task >> email_task >> csv_cleanup
