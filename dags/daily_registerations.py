import pendulum
from airflow import DAG
from airflow.models import Variable
from airflow.operators.email_operator import EmailOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
from operators.mysql_to_csv import MySqlToCsvFile

local_tz = pendulum.timezone("Asia/Kolkata")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 11, 25, tzinfo=local_tz),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

# 0900 IST
dag = DAG('daily_registrations', default_args=default_args, schedule_interval='0 9 * * *')

now = datetime.now(local_tz)
current_date_message = now.strftime("%d %B, %Y")
current_date_filename = now.strftime("%Y%m%d")

file_names = ["/tmp/daily_registrations_" + current_date_filename + ".csv"]

mysql_to_csv_registrations = MySqlToCsvFile(task_id='daily_registrations',
                                                    sql="""
                                                    -- daily registrations
                                                    SELECT 
                                                    CASE WHEN platform = "" THEN "Email-password" ELSE platform END AS platform, 
                                                    COUNT(DISTINCT email) AS registered_users
                                                    FROM users
                                                    WHERE created_at BETWEEN DATE_SUB(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 1 DAY) AND DATE_SUB(CONCAT(CURDATE(), " 23:59:59"), INTERVAL 1 DAY)
                                                    GROUP BY platform;
                                                   """,
                                                    file_name=file_names[0],
                                                    dag=dag)

date_message = "Date of generation: " + current_date_message + "<br><br>"
yesterday = (now - timedelta(1)).strftime("%d %B, %Y")

body_message = date_message + """
                            Please find attached:<br>
                                Date range: """ + yesterday + """ 00:00:00 to  """ + yesterday + """ 23:59:59 <br><br>
                                Platform wise registrations
                            """

email_task = EmailOperator(
    to=Variable.get("daily_registrations_recipients", deserialize_json=True)["to"],
    cc=Variable.get("daily_registrations_recipients", deserialize_json=True)["cc"],
    task_id='send_email',
    subject='Daily registrations',
    html_content=body_message,
    mime_charset='us-ascii',
    files=file_names,
    dag=dag)


def delete_files(file_names):
    import os
    for file in file_names:
        if os.path.exists(file):
            os.remove(file)
            print('File removed: ' + file)
        else:
            print('File doesnt exist: ' + file)


csv_cleanup_task = PythonOperator(
    task_id='csv_cleanup',
    python_callable=delete_files,
    op_kwargs={'file_names': file_names},
    dag=dag,
)

mysql_to_csv_registrations >> email_task >> csv_cleanup_task
