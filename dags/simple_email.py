"""
Code that goes along with the Airflow tutorial located at:
https://github.com/apache/airflow/blob/master/airflow/example_dags/tutorial.py
"""

from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.email_operator import EmailOperator
from airflow.operators.mysql_operator import MySqlOperator
from airflow.operators.bash_operator import BashOperator
from airflow.models import Variable

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 6, 17),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}

dag = DAG('cohort_analysis', default_args=default_args, schedule_interval=timedelta(days=0))

mysql_operator = MySqlOperator(task_id='test_mysql',
                               mysql_conn_id='tss_stage',
                               sql=['SET @rank := 1;',
                                    'SET @user_id := -1;',
                                    'DROP TABLE IF EXISTS test_tab;',
                                    """
                                    CREATE TABLE test_tab AS
                                      SELECT
                                        id                    AS order_id,
                                        CASE WHEN t.user_id = @user_id
                                          THEN @rank := 2
                                        ELSE @rank := 1
                                        END                   AS order_seq,
                                        @user_id := t.user_id AS user_id,
                                        CASE WHEN order_status IN (3,5,23)      THEN pay_amt
                                        ELSE 0
                                        END                   AS completed_sales,
                                        CASE WHEN order_status IN (1,2,8,9,11,12,13,16,18,19,20,21,25,26,27,30,31,32,34)      THEN pay_amt
                                        ELSE 0
                                        END                   AS processing_sales,
                                        CASE WHEN order_status IN (1,2,3,5,8,9,11,12,13,16,18,19,20,21,23,25,26,27,30,31,32,34)      THEN pay_amt
                                        ELSE 0
                                        END                   AS total_sales,
                                        pay_amt,
                                        order_amt,
                                        created_at,
                                        order_status
                                      FROM (
                                             SELECT *
                                             FROM orders
                                           ) t
                                      WHERE created_at > '2015-04-01 00:00:00' AND order_status NOT IN (0, 17)
                                            AND user_id IN (SELECT id
                                                            FROM users)
                                            AND id IN (SELECT order_id
                                                       FROM has_products)
                                      ORDER BY t.user_id, id;
                               """],
                               database='tss_stage',
                               dag=dag)
# t1, t2 and t3 are examples of tasks created by instantiating operators
email_task = EmailOperator(
    to='shubham.arawkar@thesouledstore.com',
    task_id='send_email_1',
    subject='Templated Subject',
    html_content="",
    dag=dag)
